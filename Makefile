all: vfirewall-monitor vfirewall-console

vfirewall-monitor:
	mkdir -p obj
	mkdir -p bin
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector -o obj/config_file.o data_access/config_file.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/logger.o tools/logger.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/tools.o tools/tools.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/database.o data_access/database.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/interfaces.o hardware/interfaces.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/kernel.o kernel/kernel.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/main.o main.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/memory.o hardware/memory.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/partitions.o hardware/partitions.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/sysinfo.o hardware/sysinfo.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/thread.o tools/thread.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/server.o kernel/server.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/firewall.o iptables/firewall.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/filter.o iptables/filter.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/mangle.o iptables/mangle.c	
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/nat.o iptables/nat.c	
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/command.o commands/command.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/command_firewall.o commands/command_firewall.c	
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/command_config.o commands/command_config.c	
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/command_system.o commands/command_system.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -fstack-protector   -o obj/match.o iptables/match.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./iptables -I ./tools -fstack-protector   -o obj/json.o tools/json.c
	gcc -o bin/vfirewall-monitor obj/config_file.o obj/logger.o obj/tools.o obj/database.o obj/interfaces.o obj/kernel.o obj/main.o obj/memory.o obj/partitions.o obj/sysinfo.o obj/thread.o obj/server.o obj/firewall.o obj/filter.o obj/mangle.o obj/nat.o obj/command.o obj/command_firewall.o obj/command_config.o obj/command_system.o obj/match.o obj/json.o -lpq -lconfig -ljansson -pthread 

vfirewall-console:
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./tools -I ./iptables -fstack-protector -o obj/console.o console/console.c
	gcc -c -g -Wall -I/usr/include/postgresql -I ./commands -I ./tools -I ./data_access -I ./hardware -I ./kernel -I ./tools -I ./iptables -fstack-protector -o obj/client.o console/client.c
	gcc -o bin/vfirewall-console obj/console.o obj/client.o obj/json.o -ljansson -lreadline

# Clean Targets
clean:
	rm -rf obj/
	rm -rf bin/
