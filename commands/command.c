#include <readline/rlstdc.h>

#include "command.h"
char * error_msg;
char * str_msg;

int init_command(const char * str_command, Command * struct_command) {
    /*
     * Inicia o processo do comando 
     * As constantes de retorno estão definidas em tools/defines.h
     */

    /** Faz o decode do JSON */
    if (decode_command(str_command, struct_command) == DECODE_COMMAND_FAILED) {
        /** Não conseguiu descodificar o JSON, retorna o erro */
        return COMMAND_FAILED;
    }

   

    /** Chegando aqui, verifica se o comando existe */
    if (command_exists(struct_command) == COMMAND_NOT_EXISTS) {
        /** Seta a mensagem de erro */
        strcpy(struct_command->error, "Comando não encontrado");
        
        /** Retorna o comando que não foi possivel executar o comando */
        return COMMAND_FAILED;
    }

    /** Verifica qual foi o comando e chama o sub comando */
    if (strcmp(struct_command->command, COMMAND_FIREWALL) == 0) {
        /** Chama o comando de firewall */
        return call_firewall_subcommand(struct_command);

    } else if (strcmp(struct_command->command, COMMAND_CONFIG) == 0) {
        /** Chama o ocmando de config */
        return call_config_subcommand(struct_command);

    } else if (strcmp(struct_command->command, COMMAND_SYSTEM) == 0) {
        /** Chama o comando de serviços */
        return call_system_subcommand(struct_command);
    }

    return COMMAND_SUCCESS;
}

/** Pega conteúdo dom socket */
int decode_command(const char * encoded_command, Command * command) {
    /** Declara as variaveis */
    JSON * json;
    int count, params_count = 0;

    /** Verifica se o comando não é null */
    if (encoded_command == NULL) {
        /** Retorna a falha, pois o comando é invalido */
        return DECODE_COMMAND_FAILED;
    }

    /** Faz o decode */
    json = json_decode(encoded_command);
    if (json == NULL) {
        /** Retorna a falha */
        return DECODE_COMMAND_FAILED;
    }

    /** Agora com o comando descodificado, passa pra struct o comando */
    for (count = 0; count < json->pos; count++) {
        if (count == 0) {
            /** Count igual a 0 copia o comando */
            strcpy(command->command, json->values[count]);
        } else if (count == 1) {
            /** Copia o subcomando */
            strcpy(command->subcommand, json->values[count]);
        } else {
            /** Copia os parametros */
            strcpy(command->params[params_count], json->values[count]);
            params_count++;
        }
    }
    /** Chegando aqui, retorna sucesso */
    return DECODE_COMMAND_SUCCESS;
}

/** Verifica se o comando existe */
int command_exists(Command * command) {
    if (strcmp(command->command, COMMAND_FIREWALL) == 0 ||
            strcmp(command->command, COMMAND_CONFIG) == 0 ||
            strcmp(command->command, COMMAND_SYSTEM) == 0) {
        return COMMAND_EXISTS;
    } else {
        return COMMAND_NOT_EXISTS;
    }

}

/*
 * Chama o subcomando de um determinado comando 
 * Enviando qual é o subcomando
 * E quais são os parametros
 */

/** Subcomando do firewall */
int call_firewall_subcommand(Command * command) {
    /** Chama a função de execução dos comandos de firewall */
    if (init_command_firewall(command) == COMMAND_FAILED) {
        /** Falha na execução do comando */
        return COMMAND_FAILED;
    } else {
        /** Tudo OK na execução, retorna sucesso */
        return COMMAND_SUCCESS;
    }
}

/** Subcomando de config */
int call_config_subcommand(Command * command) {
    if (init_command_config(command) == COMMAND_FAILED) {
        /** Falha na execução do comando */
        return COMMAND_FAILED;
    } else {
        /** Tudo OK na execução, retorna sucesso */
        return COMMAND_SUCCESS;
    }
}

/** Subcomando de system */
int call_system_subcommand(Command * command) {
    if (init_command_system(command) == COMMAND_FAILED) {
        /** Falha na execução do comando */
        return COMMAND_FAILED;
    } else {
        /** Tudo OK na execução, retorna sucesso */
        return COMMAND_SUCCESS;
    }
}