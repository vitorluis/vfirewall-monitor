/* 
 * File:   command.h
 * Author: vitor
 *
 * Este header, conterá as função para manipulação dos comandos
 * E também terá definições que guardaram quais são os comandos
 * 
 */

#ifndef COMMAND_H
#define	COMMAND_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "config_file.h"
#include "logger.h"
#include "defines.h"


/** Define a struct de comandos */
typedef struct command {
    char command[20];
    char subcommand[20];
    char params[100][100];
    char error[4096];
    char result[4096];
} Command;

#include "server.h"

/** Include dos headers que tem declarado a função dos comandos */
#include "command_system.h"
#include "command_firewall.h"
#include "command_config.h"

/** Comandos disponiveis */
#define COMMAND_FIREWALL "firewall"
#define COMMAND_CONFIG "config"
#define COMMAND_SYSTEM "system"

/** Inicia o processo de execução de um comando */
int init_command(const char * str_command, Command * command);

/** Faz o decode do comando  */
int decode_command(const char * encoded_command, Command * command);

/** Verifica se o comando existe */
int command_exists(Command * command);

/*
 * Chama o subcomando de um determinado comando 
 * Enviando qual é o subcomando
 * E quais são os parametros
 */

/** Subcomando do firewall */
int call_firewall_subcommand(Command * command);

/** Subcomando de config */
int call_config_subcommand(Command * command);

/** Subcomando de sistema */
int call_system_subcommand(Command * command);

#endif	/* COMMAND_H */

