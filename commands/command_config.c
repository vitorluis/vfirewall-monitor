#include "command_config.h"
/** Variaveis globais */
config_t conf;

int init_command_config(Command* command) {
    /** Declara algumas vars */
    char buff[300];
    
    /** Começa verificando se o comando existe */
    if (subcommand_config_exists(command) != SUBCOMMAND_EXISTS) {
        /** Informa o erro */
        strcpy(command->error, "Erro: Subcomando não Encontrado. Exemplo: config {reload|set|get}");
        
        /** Retorna a falha que o comando não existe */
        return COMMAND_FAILED;
    }

    /** Chegando aqui, verifica qual foi o subcomando e executa a tarefa */
    if (strcmp(command->subcommand, SUBCOMMAND_CONFIG_RELOAD) == 0) {
        /** Chama o reload */
        if (command_config_reload() == SUBCOMMAND_FAILED) {
            /** Deu falha no reload, seta o erro */
            strcpy(command->error, "Erro: Falha ao fazer a releitura do arquivo de configuração");

            /** Insere nos logs a falha */
            insert_log(FATAL, LOG_GENERAL, "Falha ao fazer a releitura do arquivo de configuração - command_config.c");

            /** Retorna falha */
            return COMMAND_FAILED;
        } else {
            /** Seta a mensagem */
            strcpy(command->result, "Arquivo de configuração relido com sucesso");
            
            /** Insere nos logs */
            insert_log(NOTICE, LOG_GENERAL, "Arquivo de configuração relido - command_config.c");

            /** Retorna sucesso */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_CONFIG_SET) == 0) {
        /** Executa o comando set */
        if (command_config_set(command) == SUBCOMMAND_FAILED) {
            /** Seta o erro */
            strcpy(command->error, "Erro: Não foi possivel atualizar o arquivo de configuração");

            /** Insere nos logs */
            insert_log(WARNING, LOG_GENERAL, "Falha ao atualizar arquivo de configuração - command_config.c");

            /** retorna falha na execução */
            return COMMAND_FAILED;
        } else {
            /** Seta a mensagem */
            strcpy(command->result, "Chave atualizada com sucesso");
            
            /** Insere nos logs */
            insert_log(NOTICE, LOG_GENERAL, "Arquivo de configuração atualizado - command_config.c");

            /** Retorna sucesso */
            return COMMAND_SUCCESS;
        }
    }

    /** Se for um get, insere na variavel global de resultados */
    if (strcmp(command->subcommand, SUBCOMMAND_CONFIG_GET) == 0) {
        if (command_config_get(command) != NULL) {
            /** Coloca na variavel de resultado o retorno */
            strcpy(command->result, command_config_get(command));

            /** retorna sucesso */
            return COMMAND_SUCCESS;
        } else {
            /** Salva o erro */
            snprintf(buff, sizeof(buff), "Erro: Não foi possível encontrar a chave '%s' no arquivo de configuração", command->params[0]);
            strcpy(command->error, buff);
            
            /** Retorna erro */
            return COMMAND_FAILED;
        }
    }

    /** Se chegar aqui, não entrou em nenhum if anterior */
    strcpy(command->error, "Erro: Subcomando não existe");
    return COMMAND_FAILED;
}

int subcommand_config_exists(Command * command) {
    /** Verifica se o comando existe, retornando se sim ou se não */
    if (strcmp(command->subcommand, SUBCOMMAND_CONFIG_RELOAD) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_CONFIG_SET) == 0 ||
            strcmp(command->subcommand,SUBCOMMAND_CONFIG_GET) == 0) {
        return SUBCOMMAND_EXISTS;
    } else {
        return SUBCOMMAND_NOT_EXISTS;
    }

}

const char * command_config_get(Command* command) {
    /*
     * Faz o get da chave 
     * Retorna como string
     */
    return get_config_str(&conf, command->params[0]);
}

int command_config_set(Command* command) {
    /*
     * Faz o set da chave 
     * É necessário ver se é int, float ou string
     */
    if (is_int(command->params[1])) {
        /** Se for inteiro chama a função adequada */
        if (set_config_int(&conf, command->params[0], atoi(command->params[1])) != SET_CONFIG_SUCCESS) {
            /** Se entrar aqui, não conseguiu setar o valor novo */
            return SUBCOMMAND_FAILED;
        }
    } else if (is_float(command->params[1])) {
        /** Se for float, chama a função adequada */
        if (set_config_double(&conf, command->params[0], atof(command->params[1])) != SET_CONFIG_SUCCESS) {
            /** Se entrar aqui, não conseguiu setar o valor novo */
            return SUBCOMMAND_FAILED;
        }
    } else {
        /** Se entrar aqui é uma string */
        if (set_config_str(&conf, command->params[0], command->params[1]) != SET_CONFIG_SUCCESS) {
            /** Se entrar aqui, não conseguiu setar o valor novo */
            return SUBCOMMAND_FAILED;
        }
    }

    /** Se chegar aqui, fez a inserção do novo valor, retorna sucesso */
    return SUBCOMMAND_SUCCESS;
}

int command_config_reload() {
    /** Faz o reload do arquivo */
    close_config_file(&conf);
    /** Le novamente */
    if (read_application_config(&conf) != READ_CONFIG_FILE_SUCCESS) {
        /** Não conseguiu ler o arquivo de configuração, retorna falha */
        return SUBCOMMAND_FAILED;
    }

    /** Se chegar aqui, conseguiu reler o arquivo */
    return SUBCOMMAND_SUCCESS;
}