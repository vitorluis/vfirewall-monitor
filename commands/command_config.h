/* 
 * File:   command_config.h
 * Author: vfirewall
 *
 * Created on February 3, 2014, 9:06 PM
 */

#ifndef COMMAND_CONFIG_H
#define	COMMAND_CONFIG_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "config_file.h"
#include "kernel.h"
#include "command.h"
#include "tools.h"

/** Comandos do Config */
#define SUBCOMMAND_CONFIG_RELOAD "reload"
#define SUBCOMMAND_CONFIG_SET "set"
#define SUBCOMMAND_CONFIG_GET "get"

/** Inicia a execução do comando de config */
int init_command_config(Command * command);

/** Verifica se o subcomando existe */
int subcommand_config_exists(Command * command);

/** Faz o reload do arquivo de configuração */
int command_config_reload();

/** Seta um novo valor a uma chave */
int command_config_set(Command * command);

/** Pega o valor de uma chave */
const char * command_config_get(Command * command);

#endif	/* COMMAND_CONFIG_H */

