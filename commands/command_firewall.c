#include "command_firewall.h"

config_t conf;

int init_command_firewall(Command* command) {
    /** Verifica se o subcomando existe */
    if (subcommand_firewall_exists(command) != SUBCOMMAND_EXISTS) {
        /** Seta a mensagem de erro */
        strcpy(command->error, "Erro: Subcomando não Encontrado");

        /** Retorna a falha */
        return COMMAND_FAILED;
    }

    /*
     * Chegando aqui, verifica qual foi o comando
     * e executa a chamada do mesmo
     */
    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_RELOAD) == 0) {
        /** Faz o reload do firewall */
        if (command_firewall_reload(command) == SUBCOMMAND_FAILED) {
            /** retorna a falha do comando */
            return COMMAND_FAILED;
        } else {
            /** retorna sucesso do comando */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_LOAD) == 0) {
        /** Faz o load do arquivo de regras */
        if (command_firewall_load(command) == SUBCOMMAND_FAILED) {
            /** Retorna a falha para a função pai */
            return COMMAND_FAILED;
        } else {
            /** Retorna sucesso para a função pai */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_BLOCK) == 0) {
        /** Faz o block de um IP */
        if (command_firewall_block(command) == SUBCOMMAND_FAILED) {
            /** retorna falha para a função pai */
            return COMMAND_FAILED;
        } else {
            /** retorna sucesso para a função pai */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_UNBLOCK) == 0) {
        /** Faz o desbloqueio de um IP */
        if (command_firewall_unblock(command) == SUBCOMMAND_FAILED) {
            /** retorna falha para a função pai */
            return COMMAND_FAILED;
        } else {
            /** retorna sucesso para a função pai */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_ENABLE) == 0) {
        /** Habilita o firewall */
        if (command_firewall_enable(command) == SUBCOMMAND_FAILED) {
            /** retorna falha para o comando pai */
            return COMMAND_FAILED;
        } else {
            /** retorna sucesso para a função pai */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_DISABLE) == 0) {
        if (command_firewall_disable(command) == SUBCOMMAND_FAILED) {
            /** retorna falha para o comando pai */
            return COMMAND_FAILED;
        } else {
            /** retorna sucesso para a função pai */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_SHOW) == 0) {
        if (command_firewall_show(command) == SUBCOMMAND_FAILED) {
            /** retorna falha para o comando pai */
            return COMMAND_FAILED;
        } else {
            /** retorna sucesso para a função pai */
            return COMMAND_SUCCESS;
        }
    }

    /** Caso não entre em nehum, retorna a falha na execução do comando */
    return COMMAND_FAILED;
}

int subcommand_firewall_exists(Command* command) {
    /** Faz a verificação se o comando existe */
    if (strcmp(command->subcommand, SUBCOMMAND_FIREWALL_RELOAD) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_FIREWALL_LOAD) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_FIREWALL_BLOCK) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_FIREWALL_UNBLOCK) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_FIREWALL_ENABLE) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_FIREWALL_DISABLE) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_FIREWALL_SHOW) == 0) {
        return SUBCOMMAND_EXISTS;
    } else {
        return SUBCOMMAND_NOT_EXISTS;
    }
}

int command_firewall_reload(Command * command) {
    /** Chama a função que faz o flush */
    if (flush_rules() == IPTABLES_FLUSH_FAILED) {
        /** Seta a mensagem de falha */
        strcpy(command->error, "Erro: Falha ao limpar as regras do firewall");

        /** Falha no flush, insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Falha ao limpar as regras do iptables - command_firewall.c");

        /** Retorna a falha */
        return SUBCOMMAND_FAILED;
    }

    /** Chegando aqui, faz o init do firewall novamente */
    if (init_firewall() == INIT_FIREWALL_FAILED) {
        /** Seta a mensagem de falha */
        strcpy(command->error, "Erro: Falha ao aplicar as regras no firewall");

        /** Insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Falha ao reiniciar o firewall - command_firewall.c");

        /** Retorna a falha */
        return SUBCOMMAND_FAILED;
    }

    /** Seta a mensagem de retorno */
    strcpy(command->result, "Firewall recarregado com sucesso");

    /** Se chegar aqui, conseguiu limpar e setar as regras novamente */
    return SUBCOMMAND_SUCCESS;

}

int command_firewall_load(Command* command) {
    /*
     * Faz o load de um arquivo de regras 
     * Verifica se o arquivo existe
     */
    if (strcmp(command->params[0], "") != 0) {
        /** Só entra aqui, se o parametro não for uma espaço */
        if (access(command->params[0], F_OK) == 0) {
            /** Se entrar aqui, o arquivo existe, tenta fazer o load */
            if (load_rules_file(command->params[0]) != LOAD_FILE_RULES_SUCCESS) {
                /** Seta o erro */
                strcpy(command->error, "Erro: Falha ao carregar o arquivo de regras");

                /** Retorna a falha */
                return SUBCOMMAND_FAILED;
            } else {
                /** Seta a mensagem */
                strcpy(command->result, "Arquivo de regras carregado com sucesso");

                /** Retorna sucesso */
                return SUBCOMMAND_SUCCESS;
            }
        } else {
            /** Seta a mensagem de erro */
            strcpy(command->error, "O arquivo de regras não existe");

            /** Retorna a falha */
            return SUBCOMMAND_FAILED;
        }
    } else {
        /** seta o erro */
        strcpy(command->error, "Informe o caminho do arquivo de regras");

        /** Retorna a falha */
        return SUBCOMMAND_FAILED;

    }
    return SUBCOMMAND_FAILED;
}

int command_firewall_show(Command * command) {
    /** Verifica se é show status, ou show rules */
    if (strcmp(command->params[0], "") == 0) {
        /** Seta o erro */
        strcpy(command->error, "Ação de show inválida. Exemplo: firewall show {status|blocked-ip}");

        /** retorna falha */
        return SUBCOMMAND_FAILED;
    } else if (strcmp(command->params[0], "status") == 0) {
        /** Variavel que guardara a tabela das informações */
        char status[1024];
        strcpy(status, "-------------------------\n");

        /** Verifica se o modulo está habilitado e rodando */
        char * result = execute_command("/sbin/lsmod | /bin/grep ip_tables");

        /** Verifica se o comando não retornou vazio*/
        if (result != NULL) {
            /** Concatena mensagem de firewall ativo */
            strcat(status, "Firewall: Ativo\n");
        } else {
            /** Se entrar aqui, o módulo não está ativo */
            strcat(status, "Firewall: Inativo\n");
        }

        /** Copia para a mensagem */
        strcpy(command->result, status);
        /** Retorna sucesso */
        return SUBCOMMAND_SUCCESS;
    } else if (strcmp(command->params[0], "blocked-ip") == 0) {
        /** Mostra a lista de IPS bloqueados */
        char lista[100];
        char command_ip_file[100];

        /** Monta o comando */
        snprintf(command_ip_file,
                sizeof (command_ip_file),
                "/bin/cat %s",
                get_config_str(&conf, "ip_blocked_file"));

        /** Executa o comando cat */
        if (execute_command(command_ip_file) != NULL)
            strcpy(lista, execute_command(command_ip_file));

        /** Joga na variavel de resultado, o retorno do comando */
        if (lista != NULL) {
            strcpy(command->result, lista);

            /** Retorna sucesso*/
            return SUBCOMMAND_SUCCESS;
        } else {
            /** Seta a mensagem de erro */
            strcpy(command->error, "Erro: Não foi possível encontrar o arquivo de IPs");

            /** Retorna a falha */
            return SUBCOMMAND_FAILED;
        }
    } else {
        /** Não entrou em nenhuma ação acima, reotrna falha */
        char error[100];
        snprintf(error,
                sizeof (error),
                "Ação '%s' inválida. Exemplo: firewall show {status|blocked-ip}",
                command->params[0]);
        strcpy(command->error, error);

        /** Retorna a falha */
        return SUBCOMMAND_FAILED;
    }

}

int command_firewall_block(Command * command) {
    /** Variaveis de log e erros */
    char log[100];
    char error[100];

    /** Verifica se o ip está vazio */
    if (strcmp(command->params[0], "") == 0) {
        /** Seta a mensagem de erro */
        strcpy(command->error, "Informe o endereço IP");

        /** Retorna falha */
        return SUBCOMMAND_FAILED;
    }

    /*
     * Se chegar aqui, foi informado alguma coisa 
     * Agora valida se é um IP
     */
    if (!is_valid_ip(command->params[0])) {
        /** Seta a mensagem de erro */
        snprintf(error,
                sizeof (error),
                "O endereço '%s' não é um IP válido",
                command->params[0]);

        strcpy(command->error, error);

        /** Retorna falha */
        return SUBCOMMAND_FAILED;
    }

    /*
     * Se chegar aqui, o IP é valido, faz o procedimento de block 
     * Cria as vars que vão guardar os comandos
     */
    char command_input[100];
    char command_output[100];
    char command_forward_source[100];
    char command_forward_dest[100];

    /** Formatação do comando de input */
    snprintf(command_input,
            sizeof (command_input),
            "/sbin/iptables -A INPUT --source %s -j DROP",
            command->params[0]);

    /** Formatação do comando de output */
    snprintf(command_output,
            sizeof (command_output),
            "/sbin/iptables -A OUTPUT --destination %s -j DROP",
            command->params[0]);

    /** Formata os comandos de forward */
    snprintf(command_forward_source,
            sizeof (command_forward_source),
            "/sbin/iptables -A FORWARD --source %s -j DROP",
            command->params[0]);

    /** Formatação do comando de output */
    snprintf(command_forward_dest,
            sizeof (command_forward_dest),
            "/sbin/iptables -A FORWARD --destination %s -j DROP",
            command->params[0]);

    /** Executa os comandos */
    if (system(command_input) != 0 || system(command_output) != 0 || system(command_forward_source) != 0 || system(command_forward_dest) != 0) {
        /** Se entrar deu falha na execução, seta a mensagem */
        strcpy(command->error, "Falha ao bloquear IP no firewall");

        /** Insere nos logs do sistema */
        snprintf(log,
                sizeof (log),
                "Falha ao bloquear IP %s no firewall - command_firewall.c",
                command->params[0]);
        insert_log(WARNING, LOG_FIREWALL, log);

        /** retorna falha */
        return SUBCOMMAND_FAILED;
    }
    snprintf(log, sizeof (log), "echo '%s' >> %s", command->params[0], get_config_str(&conf, "ip_blocked_file"));
    system(log);
    /** Se chegar aqui, fez todos os passos corretamente, seta a mensagem */
    strcpy(command->result, "IP Bloqueado com sucesso");

    /** Insere nos logs do sistema */
    snprintf(log,
            sizeof (log),
            "IP %s temporáriamente bloqueado - command_firewall.c",
            command->params[0]);
    insert_log(NOTICE, LOG_FIREWALL, log);

    /** insere no arquivo de ips bloqueados */


    /** Retorna sucesso */
    return SUBCOMMAND_SUCCESS;
}

int command_firewall_unblock(Command* command) {
    /** Variaveis de erros e logs */
    char log[100];
    char error[100];

    /** Verifica se o ip está vazio */
    if (strcmp(command->params[0], "") == 0) {
        /** Seta a mensagem de erro */
        strcpy(command->error, "Informe o endereço IP");

        /** Retorna falha */
        return SUBCOMMAND_FAILED;
    }

    /*
     * Se chegar aqui, foi informado alguma coisa 
     * Agora valida se é um IP
     */
    if (!is_valid_ip(command->params[0])) {
        /** Seta a mensagem de erro */
        snprintf(error,
                sizeof (error),
                "O endereço '%s' não é um IP válido",
                command->params[0]);

        strcpy(command->error, error);

        /** Retorna falha */
        return SUBCOMMAND_FAILED;
    }

    /*
     * Se chegar aqui, o IP é valido, faz o procedimento de block 
     * Cria as vars que vão guardar os comandos
     */
    char command_input[100];
    char command_output[100];
    char command_forward_source[100];
    char command_forward_dest[100];

    /** Formatação do comando de input */
    snprintf(command_input,
            sizeof (command_input),
            "/sbin/iptables -D INPUT --source %s -j DROP",
            command->params[0]);

    /** Formatação do comando de output */
    snprintf(command_output,
            sizeof (command_output),
            "/sbin/iptables -D OUTPUT --destination %s -j DROP",
            command->params[0]);

    /** Formata os comandos de forward */
    snprintf(command_forward_source,
            sizeof (command_forward_source),
            "/sbin/iptables -D FORWARD --source %s -j DROP",
            command->params[0]);

    /** Formatação do comando de output */
    snprintf(command_forward_dest,
            sizeof (command_forward_dest),
            "/sbin/iptables -D FORWARD --destination %s -j DROP",
            command->params[0]);

    /** Executa os comandos */
    if (system(command_input) != 0 || system(command_output) != 0 || system(command_forward_source) != 0 || system(command_forward_dest) != 0) {
        /** Se entrar deu falha na execução, seta a mensagem */
        strcpy(command->error, "Falha ao desbloquear IP no firewall");

        /** Insere nos logs do sistema */
        snprintf(log,
                sizeof (log),
                "Falha ao desbloquear IP %s no firewall - command_firewall.c",
                command->params[0]);
        insert_log(WARNING, LOG_FIREWALL, log);

        /** retorna falha */
        return SUBCOMMAND_FAILED;
    }

    /** Se chegar aqui, fez todos os passos corretamente, seta a mensagem */
    strcpy(command->result, "IP Desbloqueado com sucesso");

    /** Insere nos logs do sistema */
    snprintf(log,
            sizeof (log),
            "IP %s temporáriamente desbloqueado - command_firewall.c",
            command->params[0]);
    insert_log(NOTICE, LOG_FIREWALL, log);

    /** Retorna sucesso */
    return SUBCOMMAND_SUCCESS;
}

int command_firewall_enable(Command * command) {
    /** Chama o init do firewall */
    if (init_firewall() == INIT_FIREWALL_FAILED) {
        /*
         * Se entrar aqui, ocorreu algum problema com o inicio do firewall
         * Seta a mensagem */
        strcpy(command->error, "Erro: Falha ao iniciar o firewall");

        /** Insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Falha ao iniciar o modúlo do firewall - command_firewall.c");

        /** Retorna a falha */
        return SUBCOMMAND_FAILED;

    } else {
        /** Ocorreu tudo sussa, * Seta a mensagem */
        strcpy(command->result, "Firewall Iniciado com Sucesso");

        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Firewall habilitado, modulo iniciado - command_firewall.c");

        /** Retorna o sucesso para a função pai */
        return SUBCOMMAND_SUCCESS;
    }
}

int command_firewall_disable(Command * command) {
    /** Chama o flush do firewall */
    if (flush_rules() == IPTABLES_FLUSH_FAILED) {
        /**Falha ao limpar as regras, Seta a mesangem */
        strcpy(command->error, "Erro: Falha ao desabilitar o firewall");

        /** Insere nos logs */
        insert_log(WARNING, LOG_FIREWALL, "Falha ao limpar as regras do firewall - command_firewall.c");

        /** Retorna falha a função pai */
        return SUBCOMMAND_FAILED;
    } else {
        /** Conseguiu limpar as regras, seta a mesangem */
        strcpy(command->result, "Firewall desabilitado com sucesso");

        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Regras do firewall foram limpas - command_firewall.c");

        /** Retorna sucesso */
        return SUBCOMMAND_SUCCESS;
    }
}

int open_ip_file() {
    /** Se o arquivo não existir, cria o arquivo */
    int fd;
    const char * file = get_config_str(&conf, "ip_blocked_file");

    if (access(file, F_OK) != 0) {
        /** Cria o arquivo */
        fd = creat(file, O_APPEND | O_WRONLY);

        /** Seta as permissões */
        chmod(file, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    } else {
        fd = open(file, O_APPEND | O_WRONLY);
    }

    /** Retorna o file descriptor */
    return fd;
}

int write_ip_file(int fd, char* content) {
    /** Concatena um \n */
    strcat(content, "\n");

    /** Grava no arquivo */
    return write(fd, content, sizeof (content));
}

void remove_ip(const char* ip) {
    char command[100];
    snprintf(command,
            sizeof (command),
            "/bin/sed -i '/%s/d' %s",
            ip,
            get_config_str(&conf, "ip_blocked_file"));
    system(command);
}