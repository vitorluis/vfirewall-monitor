/* 
 * File:   command_firewall.h
 * Author: vfirewall
 *
 * Created on February 2, 2014, 6:43 PM
 */

#ifndef COMMAND_FIREWALL_H
#define	COMMAND_FIREWALL_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "config_file.h"
#include "logger.h"
#include "defines.h"
#include "command.h"
#include "firewall.h"
#include "kernel.h"
#include "tools.h"

/*
 * Funções que fazem a execução de 
 * comandos em relação ao firewall
 */
/** Subcomandos do firewall */
#define SUBCOMMAND_FIREWALL_RELOAD "reload"
#define SUBCOMMAND_FIREWALL_LOAD "load"
#define SUBCOMMAND_FIREWALL_SHOW "show"
#define SUBCOMMAND_FIREWALL_BLOCK "block"
#define SUBCOMMAND_FIREWALL_UNBLOCK "unblock"
#define SUBCOMMAND_FIREWALL_ENABLE "enable"
#define SUBCOMMAND_FIREWALL_DISABLE "disable"

int init_command_firewall(Command * command);

/** Verifica se o subcomando existe */
int subcommand_firewall_exists(Command * command);

/** Executa as funções para fazer o reload as regras */
int command_firewall_reload(Command * command);

/*
 * Comando para mostrar as regras ativas 
 * (direto no iptables ou a regra que está salva no banco?)
 */
int command_firewall_show(Command * command);

/** Faz um load de um arquivo de regras */
int command_firewall_load(Command * command);

/** Comando para bloquear um determinado IP */
int command_firewall_block(Command * command);

/** Comando para desbloquear um determinado IP */
int command_firewall_unblock(Command * command);

/** Comando para habilitar o firewall */
int command_firewall_enable(Command * command);

/** Comando para desabilitar o firewall */
int command_firewall_disable(Command * command);

/** Abre o arquivo e retorna o file descriptor */
int open_ip_file();

/** Insere ip no arquivo */
int write_ip_file(int fd, char * content);

/** Remove o endreço IP da lista */
void remove_ip(const char * ip);
#endif	/* COMMAND_FIREWALL_H */

