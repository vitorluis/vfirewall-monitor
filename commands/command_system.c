#include "command_system.h"
config_t conf;

int init_command_system(Command* command) {
    /** Verifica se o subcommand existe */

    if (subcommand_system_exists(command) != SUBCOMMAND_EXISTS) {
        /** Seta a mensagem de erro */
        strcpy(command->error, "Erro: Subcomando não encontrado");
        /** Não existe, retorna a falha para a função pai */
        return COMMAND_FAILED;
    }

    if (strcmp(command->subcommand, SUBCOMMAND_SYSTEM_SHOW) == 0) {
        /** Executa o subcomando de show */
        if (command_system_show(command) != SUBCOMMAND_SUCCESS) {
            /** Caso de falha, retorna pra função pai a falha */
            return COMMAND_FAILED;
        } else {
            /** Seta o resultado */
            strcpy(command->result, "Fechado o arquivo de log");

            /** Retorna sucesso */
            return COMMAND_SUCCESS;
        }
    }

    if (strcmp(command->subcommand, SUBCOMMAND_SYSTEM_RELOAD) == 0) {
        if (command_system_reload(command) != SUBCOMMAND_SUCCESS) {
            /** Caso de falha, retorna pra função pai a falha */
            return COMMAND_FAILED;
        } else {
            /** Retorna sucesso */
            return COMMAND_SUCCESS;
        }
    }

    /** Seta a mensagem de erro, e retorna falha */
    strcpy(command->error, "Erro: Subcomando não encontrado");
    return COMMAND_FAILED;
}

int subcommand_system_exists(Command* command) {
    /** Verifica se o subcomando passado realmente existe */
    if (strcmp(command->subcommand, SUBCOMMAND_SYSTEM_RELOAD) == 0 ||
            strcmp(command->subcommand, SUBCOMMAND_SYSTEM_SHOW) == 0) {
        return SUBCOMMAND_EXISTS;
    } else {
        return SUBCOMMAND_NOT_EXISTS;
    }
}

int command_system_reload(Command* command) {
    /** Faz o reload de todo o sistema */
    if (reload_system() == RELOAD_SYSTEM_FAILED) {
        /** Seta a mensagem de erro */
        strcpy(command->error, "Erro: Não foi possível fazer o reload da aplicação");

        /** Insere nos logs o erro */
        insert_log(FATAL, LOG_GENERAL, "Falha ao fazer o reload da aplicação - command_system.c");

        /** retorna a falha */
        return SUBCOMMAND_FAILED;
    } else {
        /** Seta a mensagem de retorno */
        strcpy(command->result, "Reload da aplicação concluido com sucesso");

        /** Insere nos logs o sucesso */
        insert_log(NOTICE, LOG_GENERAL, "Reload da aplicação executada - command_system.c");

        /** Retorna o sucesso */
        return SUBCOMMAND_SUCCESS;
    }
}

int command_system_show(Command* command) {
    /** Verifica se o parametro é o log */
    if (strcmp(command->params[0], "log") == 0) {
        char file[100];

        /** É um log, verifica qual é o arquivo de log a ser aberto */
        if (strcmp(command->params[1], "kernel")) {
            snprintf(file,
                    sizeof (file),
                    "/usr/bin/less %skernel.log",
                    get_config_str(&conf, "logpath"));
        } else if (strcmp(command->params[1], "firewall")) {
            /** Abre o log do firewall */
            snprintf(file,
                    sizeof (file),
                    "/usr/bin/less %sfirewall.log",
                    get_config_str(&conf, "logpath"));
        } else if (strcmp(command->params[1], "sysinfo")) {
            /** Abre o log de informações do sistema */
            snprintf(file,
                    sizeof (file),
                    "/usr/bin/less %ssysinfo.log",
                    get_config_str(&conf, "logpath"));
        } else if (strcmp(command->params[1], "database")) {
            /** Abre o log com informações do database */
            snprintf(file,
                    sizeof (file),
                    "/usr/bin/less %sdatabase.log",
                    get_config_str(&conf, "logpath"));
        } else if (strcmp(command->params[1], "interfaces")) {
            /** Abre o log com informações das interfaces de rede */
            snprintf(file,
                    sizeof (file),
                    "/usr/bin/less %sinterfaces.log",
                    get_config_str(&conf, "logpath"));
        } else {
            /** Se não entrar em nenhum, abre o log geral */
            snprintf(file,
                    sizeof (file),
                    "/usr/bin/less %sgeneral.log",
                    get_config_str(&conf, "logpath"));
        }

        /** Abre o log */
        system(file);

        /** retorna sucesso */
        return SUBCOMMAND_SUCCESS;


    } else {
        /** Não é um log, seta o erro */
        strcpy(command->error, "Sintaxe incorreta. Exemplo: system show log {kernel|interfaces|sysinfo|database|firewall}");

        /** retorna falha */
        return SUBCOMMAND_FAILED;
    }
}
