/* 
 * File:   command_system.h
 * Author: vitor
 *
 * Created on February 12, 2014, 11:21 AM
 */

#ifndef COMMAND_SYSTEM_H
#define	COMMAND_SYSTEM_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include "config_file.h"
#include "kernel.h"
#include "command.h"
#include "tools.h"
#include "database.h"

/** Comandos do System */
#define SUBCOMMAND_SYSTEM_RELOAD "reload"
#define SUBCOMMAND_SYSTEM_SHOW "show"

/** Init do comando */
int init_command_system(Command * command);

/** Verifica se o subcomando existe */
int subcommand_system_exists(Command* command);

/** Comando de reload */
int command_system_reload(Command * command);

/** Comando para mostra algo */
int command_system_show(Command * command);
#endif	/* COMMAND_SYSTEM_H */

