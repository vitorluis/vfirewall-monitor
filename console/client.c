#include "client.h"

int connect_server(Client * cli) {
   
    /** Inicializa o buffer como 0 */
    memset(cli->command, 0, sizeof (cli->command));
    memset(cli->response, 0, sizeof (cli->response));

    /** E o sockaddr_in também como 0 */
    memset(&cli->server, 0, sizeof (cli->server));

    /** Tenta criar o socket */
    cli->sock_fd = socket(AF_INET, SOCK_STREAM, 0);

    /** Verifica se abriu, caso contrario, retorna falha */
    if (cli->sock_fd < 0) {
        /** Informa o erro */
        fprintf(stderr, "Erro: Não foi possivel abrir o socket\n");
        
        /** Retorna NULL, indicando que não foi possivel abrir a conexão */
        return CONNECT_FAILED;
    }

    /** Aqui, continua com o processo de conexão */
    cli->server.sin_family = AF_INET;
    cli->server.sin_port = htons(2200);

    /** Seta o IP */
    if (inet_pton(AF_INET, cli->ip, &cli->server.sin_addr) <= 0) {
        /** Informa o erro */
        fprintf(stderr, "Erro: Endereço de IP inválido\n");

        /** Fecha o socket */
        close(cli->sock_fd);
        
        /** Retorna NULL, indicando que falhou na conexão */
        return CONNECT_FAILED;
    }

    /** Tenta fazer o connect */
    if (connect(cli->sock_fd, (struct sockaddr*) &cli->server, sizeof (cli->server)) < 0) {
        /** Informa o erro */
        fprintf(stderr, "Erro: Não foi possivel conectar no servidor\n");
        
        /** Fecha o socket */
        close(cli->sock_fd);

        /** Retorna NULL, indicando que falhou na conexão */
        return CONNECT_FAILED;
    }

    /** Tudo com sucesso, retorna a struct do client */
    return CONNECT_SUCCESS;
}

int send_command(Client * cli) {
    /** Tenta fazer o write no socket */
    if (write(cli->sock_fd, cli->command, sizeof(cli->command)) == -1) {
        /** Falha ao enviar o comando */
        return SEND_COMMAND_FAILED;
    }
    /** Se conseguiu, beleza */
    return SEND_COMMAND_SUCCESS;
}

int recv_response(Client * cli) {
    /** Pega a resposta, e grava na struct */
    if (read(cli->sock_fd, cli->response, sizeof(cli->response)) == -1) {
        /** Falha ao ler a resposta, retorna falha */
        return RECV_RESPONDE_FAILED;
    }
    /** Chegando aqui beleza, conseguiu ler */
    return RECV_RESPONDE_SUCCESS;
}

void close_server_connection(Client * cli) {
    close(cli->sock_fd);
}