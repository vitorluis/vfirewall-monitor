/* 
 * File:   client.h
 * Author: vitor
 *
 * Created on February 24, 2014, 8:58 AM
 */

#ifndef CLIENT_H
#define	CLIENT_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

/** Constantes para definir status do socket client */
#define CONNECT_SUCCESS         100
#define CONNECT_FAILED         -100
#define SEND_COMMAND_SUCCESS    101
#define SEND_COMMAND_FAILED    -101
#define RECV_RESPONDE_SUCCESS   102
#define RECV_RESPONDE_FAILED   -102

/** Cria a struct do client */
typedef struct client_socket {
    int sock_fd;
    struct sockaddr_in server;
    char ip[20];
    int port;
    char command[300];
    char response[4096];
} Client;

/** Conecta com o client */
int connect_server(Client * cli);

/** Envia o comando */
int send_command(Client * cli);

/** Recebe a resposta */
int recv_response(Client * cli);

void close_server_connection(Client * cli);

#endif	/* CLIENT_H */

