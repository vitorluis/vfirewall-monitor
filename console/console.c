#include "console.h"

int init_console(char * ip, int port) {
    /** Variaveis que serão utilizadas */
    char * str_command;
    char * filename = malloc(100);
    Client * cli = malloc(sizeof (Client));

    /** Monta o filename do arquivo de histórico */
    strcpy(filename, getenv("HOME"));
    strcat(filename, HISTORY_FILE);
    /** Registra o ctrl-c*/
    register_signal();

    /** Inicia a readline */
    init_readline();

    /** Cria o arquivo de historico */
    if (access(filename, F_OK) != 0) {
        /** Se  o arquivo não existe, então cria ele */
        create_history_file(filename);
    } else {
        /** Le o arquivo de histórico */
        read_history(filename);
    }

    /** Conecta com o server */
    strcpy(cli->ip, ip);
    cli->port = port;

    if (connect_server(cli) == CONNECT_FAILED) {
        /** Falha ao conectar, retorna erro */
        return -1;
    }

    /** Pega o primeiro comando */
    while (true) {
        /** Pega o comando */
        str_command = readline(PROMPT);

        if (!str_command) {
            /** A libreadline recebeu um EOF, faz o procedimento de saída */
            exit_console(filename, cli);

            /** Saí do loop */
            break;
        }

        /** Adiciona no histórico */
        add_history(str_command);

        /** Verifica se o comando é o exit pra sair */
        if (strcmp(str_command, "exit") == 0 || strcmp(str_command, "quit") == 0) {
            /** Chama a função de saída */
            exit_console(filename, cli);

            /** Saí do loop */
            break;
        }

        /** Verifica se é um help */
        if (strcmp(str_command, "help") == 0) {
            /** Imprime o helper */
            usage();

            /** Volta para o começo do loop */
            continue;
        }

        /** Faz o encode */
        strcpy(cli->command, encode_command(str_command));

        /** Envia para o server */
        if (send_command(cli) == SEND_COMMAND_FAILED) {
            /** Falha ao enviar o comando */
            fprintf(stderr, "Não foi possível executar o comando\n");
        }

        /** Espera a resposta */
        if (recv_response(cli) == RECV_RESPONDE_FAILED) {
            /** Informa qual foi o erro */
            fprintf(stderr, "Não foi possível receber a resposta do socket\n");
        } else {
            /** Imprime qual foi o resultado */
            printf("%s\n", cli->response);
        }

        /** Limpa o buffer */
        memset(cli->response, 0, sizeof (cli->response));
    }

    return 0;
}

void create_history_file(char * filename) {
    /** Declara as variaveis */
    char dir[50];

    /** Copia qual é o diretório onde deve ficar o arquivo */
    strcpy(dir, filename);

    /** Cria o diretório */
    mkdir(dirname(dir), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    /** Mesmo se o diretório foi criado ou não, verifica se o arquivo existe */
    if (access(filename, F_OK) != 0)
        /** Se não existe, então cria ele */
        creat(filename, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
}

const char * encode_command(char* str_command) {
    /** Declara as variaveis da função */
    JSON * json = malloc(sizeof (JSON));
    char * token;
    char param[50];
    int count = 0, param_count = 1;

    /** Destrincha o comando para ser criado o JSON do mesmo */
    while ((token = strsep(&str_command, " ")) != NULL) {
        if (strcmp(token, "") != 0) {
            if (count == 0) {
                /** comando */
                strcpy(json->keys[count], "command");
                strcpy(json->values[count], token);
            } else if (count == 1) {
                /** Subcomando */
                strcpy(json->keys[count], "subcommand");
                strcpy(json->values[count], token);
            } else {
                /** Parametro */
                snprintf(param, sizeof (param), "param%d", param_count);
                strcpy(json->keys[count], param);
                strcpy(json->values[count], token);
                param_count++;
            }
            count++;
        }
    }
    json->pos = count - 1;
    return json_encode(json);
}

void usage() {
    /** Imprime o helper do console */
    printf("==== Console VFirewall ====\n\n");
    printf("Comandos Disponíveis: \n");
    printf(" * config - Comando para manipulação do arquivo de configuração\n");
    printf(" * firewall - Comando para manipulação firewall\n");
    printf(" * system - Comando para exibir informações do sistema\n\n");

    /** Detalhando o comando config */
    printf(" == Comando config\n");
    printf("Subcomandos Disponíveis: \n");
    printf(" * reload - Faz a releitura do arquivo de configuração\n");
    printf(" * set - Seta um novo valor para uma chave no arquivo\n");
    printf(" * get - Pega o valor de uma determinada chave\n\n");

    /** Detalhando o comando firewall */
    printf(" == Comando firewall\n");
    printf("Subcomandos Disponíveis: \n");
    printf(" * reload - Recarrega todas as regras do firewall\n");
    printf(" * load 'filename' - Carrega um arquivo de regras, onde 'filename' é o caminho do arquivo\n");
    printf(" * disable - Desabilita o firewall, se mantendo as regras padrões\n");
    printf(" * enable - Habilita o firewall, carregando todas as regras\n");
    printf(" * block 'ip' - Bloqueia um determinado IP, indicado por 'ip', no firewall\n");
    printf(" * unblock 'ip' -  Desbloqueia um determinado IP, indicado por 'ip', no firewall\n\n");

    /** Detalhando o comando system */
    printf(" == Comando system\n");
    printf("Subcomandos Disponíveis: \n");
    printf(" * reload - Recarrega toda a aplicação\n");
    printf(" * show 'info' - Mostra uma determinada informação, informada por 'info'\n\n");
}

void exit_console(char * filename, Client * cli) {
    /** Escreve o arquivo de histórico */
    write_history(filename);

    /** Fecha a conexão */
    close_server_connection(cli);

    /** Libera memória */
    free(cli);

    /** Sai do loop */
    printf("\nAté mais!\n");
}

void init_readline() {
    /** Inicializa o histórico de comandos */
    using_history();

    /** Inicializa o readline */
    rl_initialize();

    /** Seta o número de comandos a serem guardados */
    stifle_history(10);

    /** Habilita o auto completation */
    rl_inhibit_completion = true;
}

void register_signal() {
    /** Tenta registrar no kernel o signal */
    signal(SIGINT, signal_exit_console);
}

void signal_exit_console() {
    printf("\nAté mais!\n");
    exit(-1);
}

int main(int argc, char ** argv) {
    return init_console(argv[1], atoi(argv[2]));
}
