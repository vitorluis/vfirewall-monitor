/* 
 * File:   console.h
 * Author: vitor
 *
 * Created on February 21, 2014, 9:53 PM
 */

#ifndef CONSOLE_H
#define	CONSOLE_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <libgen.h>
#include <fcntl.h>
#include <signal.h>
#include "json.h"
#include "client.h"

#define PROMPT "vfirewall> "
#define HISTORY_FILE "/.vfirewall/history"

/** Inicia a console */
int init_console(char * ip, int port);

/** Codifica o comando */
const char * encode_command(char * str_command);

/** salva no histórico o comando */
int save_history();

/** Imprime o manual de help */
void usage();

/** Cria o arquivo de histórico */
void create_history_file(char * filename);

/** Faz o procedimento de saída do console */
void exit_console(char * filename, Client * cli);

/** Saída por sinal do kernel */
void signal_exit_console();

/** Faz o inicio da readline */
void init_readline();

/** Função que faz o registro do signal ctrl-c no kernel */
void register_signal();

#endif	/* CONSOLE_H */

