#include "config_file.h"
#include "kernel.h"

int read_config_file(config_t* cfg) {
    /*
     * Inicio da leitura do arquivo de configuração 
     * Chama o  config_init para iniciar a leitura
     */
    /** Verifica se o arquivo de configuração existe */

    config_init(cfg);

    /** Verifica se o arquivo existe */
    if (access(CONFIG_PATH, F_OK) != 0)
        return READ_CONFIG_FILE_FAILED;

    if (!config_read_file(cfg, CONFIG_PATH)) {
        /*
         * Se entrar aqui, não conseguiu fazer
         * a leitura do arquivo de configuração
         * Libera memória
         */
        /** TODO: Logar o Erro aqui */
        config_destroy(cfg);

        /** Retorna falha */
        return READ_CONFIG_FILE_FAILED;
    }

    /*
     * Se chegar aqui, conseguiu fazer a 
     * leitura do arquivo de configuração
     * retorna sucesso
     */
    return READ_CONFIG_FILE_SUCCESS;
}

void close_config_file(config_t* cfg) {
    /** Fecha o arquivo de configuração */
    config_destroy(cfg);
}

const char * get_config_str(config_t* cfg, const char* property) {
    /** Cria e aloca um char * */
    const char * value;
    value = malloc(sizeof (char) * 50);

    /** Tenta pegar o valor da propriedade */
    if (config_lookup_string(cfg, property, &value)) {
        /** Se entrou aqui, conseguiu pegar o valor */
        return value;
    } else {
        /** Se não, volta NULL */
        return NULL;
    }

}

int get_config_int(config_t* cfg, const char* property) {
    /** Cria e aloca um int */
    int value;

    /** Tenta pegar o valor da propriedade */
    if (config_lookup_int(cfg, property, &value)) {
        /** Se entrou aqui, então conseguiu pegar o valor */
        return value;
    }
    return value;
}

double * get_config_double(config_t* cfg, const char* property) {
    /** Cria e aloca um int */
    double * value;
    value = (double *) malloc(sizeof (double));

    /** Tenta pegar o valor da propriedade */
    if (config_lookup_float(cfg, property, value)) {
        /** Se entrou aqui, então conseguiu pegar o valor */
        return value;
    } else {
        /** Se não, volta NULL */
        return NULL;
    }
}

int set_config_str(config_t * cfg, const char* property, const char* value) {
    /** Cria um objeto config_setting para setar a nova config */
    config_setting_t * new_config;

    /** Pega a configuração antes de atualizada */
    new_config = config_lookup(cfg, property);
    
    /** faz a modificação */
    config_setting_set_string(new_config, value);
    
    /** Salva as alterações */
    if (config_write_file(cfg, CONFIG_PATH) == CONFIG_FALSE)  {
        /** Falha ao salvar as alterações */
        return SET_CONFIG_FAILED;
    }
    
    /** Chegando aqui, conseguiu salvar, retorna sucesso */
    return SET_CONFIG_SUCCESS;
}

int set_config_int(config_t * cfg, const char* property, int value) {
    /** Cria um objeto config_setting para setar a nova config */
    config_setting_t * new_config;

    /** Pega a configuração antes de atualizada */
    new_config = config_lookup(cfg, property);
    
    /** faz a modificação */
    config_setting_set_int(new_config, value);
    
    /** Salva as alterações */
    if (config_write_file(cfg, CONFIG_PATH) == CONFIG_FALSE)  {
        /** Falha ao salvar as alterações */
        return SET_CONFIG_FAILED;
    }
    
    /** Chegando aqui, conseguiu salvar, retorna sucesso */
    return SET_CONFIG_SUCCESS;
}

int set_config_double(config_t * cfg, const char* property, double value) {
    /** Cria um objeto config_setting para setar a nova config */
    config_setting_t * new_config;

    /** Pega a configuração antes de atualizada */
    new_config = config_lookup(cfg, property);
    
    /** faz a modificação */
    config_setting_set_float(new_config, value);
    
    /** Salva as alterações */
    if (config_write_file(cfg, CONFIG_PATH) == CONFIG_FALSE)  {
        /** Falha ao salvar as alterações */
        return SET_CONFIG_FAILED;
    }
    
    /** Chegando aqui, conseguiu salvar, retorna sucesso */
    return SET_CONFIG_SUCCESS;
}
