/* 
 * File:   config_file.h
 * Author: fastway
 *
 * Created on October 2, 2013, 12:48 PM
 */

#ifndef CONFIG_FILE_H
#define	CONFIG_FILE_H
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <libconfig.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include "defines.h"
#define CONFIG_PATH "/etc/vfirewall/vfirewall.conf"

/** Var global */
extern config_t conf;

int read_config_file(config_t * cfg);

const char * get_config_str(config_t * cfg, const char * property);

int get_config_int(config_t * cfg, const char * property);

double * get_config_double(config_t * cfg, const char * property);

int reload_config_file(config_t * cfg);

int set_config_str(config_t * cfg, const char * property, const char * value);

int set_config_int(config_t * cfg, const char * property, int value);

int set_config_double(config_t * cfg, const char * property, double value);

void close_config_file(config_t * cfg);

#endif	/* CONFIG_FILE_H */

