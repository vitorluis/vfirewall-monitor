#include "database.h"
#include "defines.h"

int open_connection(DBConnection* conn) {

    char str_conn[200];

    /*
     * Monta a string de Conexão
     */
    snprintf(str_conn, sizeof(str_conn),
            "host=%s user=%s port=%d password=%s dbname=%s",
            conn->host,
            conn->user,
            conn->port,
            conn->passwd,
            conn->dbname);

    /*
     * Tenta abrir a conexão
     */
    conn->conn_handle = PQconnectdb(str_conn);

    /*
     * Se a abertura da conexão falhar
     * retorna o erro
     */
    if (PQstatus(conn->conn_handle) != CONNECTION_OK) {
        /** Coloca no Log */
        char msg[400];
        strcpy(msg, "Falha ao Conectar no banco - ");
        strcat(msg, PQerrorMessage(conn->conn_handle));
        msg[strlen(msg) - 5] = '\0';
        strcat(msg, " - database.c");
        insert_log(FATAL, LOG_DATABASE, msg);
        PQfinish(conn->conn_handle);
        return DB_CONNECT_FAILED;
    }

    /*
     * Se chegar aqui, a conexão foi aberta
     */
    return DB_CONNECT_SUCCESS;
}

int execute_query(DBConnection* conn, Query* query) {
    /*
     * Executa a query
     * A SQL já deve estar setada na struct
     * Se não estiver, retorna false
     */
    if (query->sql == NULL) {
        /** Insere no log */
        insert_log(ERROR, LOG_DATABASE, "Query SQL vazia");

        /** Retorna falha */
        return QUERY_EXECUTE_FAILED;
    }

    /*
     * Executa query sql
     */
    query->resultset = PQexec(conn->conn_handle, (const char *) query->sql);

    /*
     * Verifica qual foi o resultado da query
     */
    if (PQresultStatus(query->resultset) == PGRES_COMMAND_OK)
        return QUERY_EXECUTE_SUCCESS;
    else if (PQresultStatus(query->resultset) == PGRES_TUPLES_OK)
        return QUERY_EXECUTE_SUCCESS;
    else {
        /** Insere nos Logs */
        char msg[500];
        strcpy(msg, "Falha na execução da Query - ");
        strcat(msg, PQerrorMessage(conn->conn_handle));
        strcat(msg, " - database.c");
        insert_log(NOTICE, LOG_DATABASE, msg);
        PQclear(query->resultset);

        /** Retorna falha */
        return QUERY_EXECUTE_FAILED;
    }

}

Row * fetch(Query* query) {
    /*
     * Aqui é o procedimento para retornar as 
     * linhas de uma consulta, que vai ser uma lista ligada
     */
    if (PQresultStatus(query->resultset) != PGRES_TUPLES_OK)
        return NULL; //Se entrar aqui é que a query não foi um select


    /*
     * Se chegar aqui foi um select
     * Então, aloca uma row
     */
    Row * row;
    /*
     * Pega o número de linhas afetadas
     */

    int rows = PQntuples(query->resultset);

    /*
     * Verifica se o numero de linhas é maior que zero 
     * Se for menor, retorna null
     */
    if (rows <= 0) {
        return NULL;
    }

    /** Se chegar aqui, aloca row */
    row = malloc(sizeof (Row));

    /*
     * Pega o número de colunas e 
     * seta na struct row
     */
    row->total_cols = PQnfields(query->resultset);

    int row_atual, col_atual; //Linha e coluna atual, vai servir para loop

    /*
     * Cria a variavel auxiliar
     * E já aponta pro começo da lista
     */
    Row * aux;
    aux = row;

    /*
     * Faz o loop para pegar os valores
     */
    for (row_atual = 0; row_atual < rows; row_atual++) {

        aux->cell = malloc(sizeof (char*) * aux->total_cols);

        /** Atribui os valores a matriz cell */
        for (col_atual = 0; col_atual < aux->total_cols; col_atual++) {
            aux->cell[col_atual] = PQgetvalue(query->resultset, row_atual, col_atual);
        }
        /** Aloca o proximo node */
        aux->next_line = malloc(sizeof (Row));
        aux = aux->next_line;
        aux->next_line = NULL;

        /** Total de colunas */
        aux->total_cols = PQnfields(query->resultset);
    }

    /*
     * Retorna o inicio da lista ligada
     */
    return row;
}

void clear_query(Query* query) {
    PQclear(query->resultset);
}

void free_row(Row* row) {
    Row * aux;

    while (row != NULL) {
        aux = row;
        row = row->next_line;
        free(aux);
    }
}

void close_connection(DBConnection* conn) {
    PQfinish(conn->conn_handle);
}