#ifndef DATABASE_H
#define	DATABASE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpq-fe.h>
#include "defines.h"
#include "logger.h"

typedef struct {
    PGconn * conn_handle;
    char host[20];
    char user[10];
    char passwd[10];
    char dbname[15];
    int port;
} DBConnection;

typedef struct {
    PGresult * resultset;
    char sql[300];
} Query;

typedef struct row {
    int total_cols;
    char ** cell;
    struct row * next_line;
} Row;

int open_connection(DBConnection * conn);

int execute_query(DBConnection * conn, Query * query);

void close_connection(DBConnection * conn);

void clear_query(Query * query);

void free_row(Row * row);

Row * fetch(Query * query);

/** Variaveis Globais */
extern DBConnection * conn;
#endif	/* DATABASE_H */

