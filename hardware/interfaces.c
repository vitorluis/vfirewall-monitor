#include "interfaces.h"

int get_interfaces_info(Interface* iface) {
    /*
     * Essa é a função que vai pegar as informações
     * Chama a função getifaddrs para pegar as informações
     */

    /* Agora pega a struct com as informações das
     * interfaces já configuradas
     */
    struct ifaddrs * ifaddr;
    if (getifaddrs(&ifaddr) == -1) {
        /** Insere nos logs a falha */
        insert_log(FATAL, LOG_INTERFACES, "Falha na syscall getifaddrs()");

        /** Retorna a falha */
        return GET_INFO_IFACES_FAILED;
    }

    /*  Se chegar aqui, conseguiu chamar a função com sucesso
     *  Chama as outras funções para pegar as infos separadamente
     *  Faz o loop para rodar a lista ligada
     */

    struct iface * iface_aux; //Variavel que irá criar os proximos nós da lista
    struct ifaddrs * ifaddr_aux; //Variavel auxiliar das ifaddrs
    struct sockaddr_in * ip; //Variavel para pegar o IP das interfaces
    struct sockaddr_in * netmask; //Variavel para pegar o Netmask das interfaces
    struct sockaddr_in * broadcast; //Variavel para pegar o Broadcast das interfaces

    /*
     * Pega todas as interfaces
     * estando configuradas ou não
     *
     *
     * A variavel auxiliar recebe o começo
     * da lista
     */
    iface_aux = iface;

    for (ifaddr_aux = ifaddr; ifaddr_aux->ifa_next != NULL; ifaddr_aux = ifaddr_aux->ifa_next) {
        /*
         * Faz o cast para saber a familia
         */
        ip = (struct sockaddr_in *) ifaddr_aux->ifa_addr;

        /** Verifica se o IP for NULL, caso seja volta pro LOOP */
        if (ip == NULL) {
            /** Entrando aqui, quer dizer que é ppp */
            strcpy(iface_aux->interface_name, ifaddr_aux->ifa_name);

            /** Faz o mesmo procedimento coloca como inativa*/
            iface_aux->interface_active = INTERFACE_INACTIVE;

            /** Copia o nome */
            strcpy(iface_aux->interface_mac_addr, "00:00:00:00:00:00");

            /** Aloca a proxima */
            iface_aux->next_interface = malloc(sizeof (Interface));

            /** Passa para a proxima */
            iface_aux = iface_aux->next_interface;

            /** Aloca a proxima como NULL */
            iface_aux->next_interface = NULL;

            /** Volta pro loop*/
            continue;
        }

        if (ip->sin_family == AF_PACKET) {
            /*
             * Seta o nome da placa
             */
            strcpy(iface_aux->interface_name, ifaddr_aux->ifa_name);

            /** Inicia os IPs como vazio */
            strcpy(iface_aux->interface_addr_ipv4, "");
            strcpy(iface_aux->interface_addr_ipv6, "");
            strcpy(iface_aux->interface_netmask, "");
            strcpy(iface_aux->interface_broadcast, "");

            /*
             * Pega o MAC da placa
             */
            get_interface_mac_addr(iface_aux);

            /*
             * Seta todo mundo como inativo,
             * o proximo loop vai acertar quem
             * estiver ativo
             */
            iface_aux->interface_active = INTERFACE_INACTIVE;

            /*
             * Vai para a proxima interface
             * Aloca a proxima interface
             */
            iface_aux->next_interface = malloc(sizeof (Interface));

            /*
             * Coloca ela como interface principal de trabalho
             */
            iface_aux = iface_aux->next_interface;
            iface_aux->next_interface = NULL;
        }
    }
    /*
     * Ele sempre aloca um nó a mais, então, o ultimo
     * nó, coloca NULL
     */
    iface_aux = NULL;
    /*
     * Agora o iface, deve apontar para a primeira interface, para tentar pegar os IPs
     */
    free(iface_aux);

    iface_aux = iface; //Volta o ponteiro para a primeira interface da lista
    /** Variavel para logs */
    char log[200];

    for (ifaddr_aux = ifaddr; ifaddr_aux->ifa_next != NULL; ifaddr_aux = ifaddr_aux->ifa_next) {
        /*
         * Faz o cast para a struct sockaddr_in
         */
        ip = (struct sockaddr_in *) ifaddr_aux->ifa_addr;
        netmask = (struct sockaddr_in *) ifaddr_aux->ifa_netmask;
        broadcast = (struct sockaddr_in *) ifaddr_aux->ifa_ifu.ifu_broadaddr;
        /*
         * Se entrar aqui nesse if, quer dizer
         * que o ip é um IPV4
         */

        /** Verifica se o IP for NULL, caso seja volta pro LOOP */
        if (ip == NULL)
            continue;

        if (ip->sin_family == AF_INET) {
            /** Verifica se a interface é realmente a que tem ser trabalhada */
            if (strcmp(iface_aux->interface_name, ifaddr_aux->ifa_name) != 0) {
                /** Insere nos logs */
                snprintf(log, sizeof (log), "Interface %s está sem endereço IP - interfaces.c", iface_aux->interface_name);
                insert_log(WARNING, LOG_INTERFACES, log);

                /** Se entrar aqui, não é essa interface, passa para a proxima */
                iface_aux = iface_aux->next_interface;
            }


            /*
             * Se entrar aqui a interface está configurada
             */
            iface_aux->interface_active = INTERFACE_ACTIVE;

            /*
             * Pega o IP
             */

            /** Variavel mensagem de log */
            char msg[200];

            if (get_interface_ifaddr(iface_aux, ip) == GET_IFADDR_FAILED) {
                /** Insere no Log */

                strcpy(msg, "Falha ao pegar o endereço IP - Interface ");
                strcat(msg, iface_aux->interface_name);
                strcat(msg, " - interfaces.c");
                insert_log(FATAL, LOG_INTERFACES, msg);

                /** Retorna a falha */
                return GET_INFO_IFACES_FAILED;
            }

            /*
             * Pega a netmask
             */
            if (get_interface_netmask(iface_aux, netmask) == GET_NETMASK_FAILED) {
                /** Insere no log */
                strcpy(msg, "Falha ao recuperar a netmask - Interface ");
                strcat(msg, iface_aux->interface_name);
                strcat(msg, " - interfaces.c");
                insert_log(WARNING, LOG_INTERFACES, msg);


                /** Retorna a falha */
                return GET_INFO_IFACES_FAILED;
            }

            /*
             * Pega o broadcast
             */
            if (get_interface_broadcast(iface_aux, broadcast) == GET_BROADCAST_FAILED) {
                /** Insere no log */
                strcpy(msg, "Falha ao recuperar o broadcast - Interface ");
                strcat(msg, iface_aux->interface_name);
                strcat(msg, " - interfaces.c");
                insert_log(WARNING, LOG_INTERFACES, msg);

                /** Retorna a falha */
                return GET_INFO_IFACES_FAILED;
            }

            /*
             * Passa para a proxima struct Interface
             * Coloca ela como interface principal de trabalho
             */

            iface_aux = iface_aux->next_interface;
        }
    }

    /*
     * Libera memória
     */

    freeifaddrs(ifaddr);

    return GET_INFO_IFACES_SUCCESS;
}

int get_interface_ifaddr(Interface * iface, struct sockaddr_in * ip) {
    /*
     * Agora transforma o IP que está em bytes
     * para IP legível com numeros e pontos
     */
    char temp[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &ip->sin_addr, temp, INET_ADDRSTRLEN);
    /*
     * Caso der erro na hora de pegar transformar o IP retorna o erro
     */
    if (temp == NULL)
        return GET_IFADDR_FAILED;

    strcpy(iface->interface_addr_ipv4, (const char *) temp);

    /*
     * Se chegar aqui os dados foram 
     * recuperados com sucesso
     */
    return GET_IPADDR_SUCCESS;
}

int get_interface_netmask(Interface * iface, struct sockaddr_in * netmask) {
    /*
     * Agora transforma a Netmask que está em bytes
     * para IP legível com numeros e pontos
     */
    char * temp = inet_ntoa(netmask->sin_addr);

    /*
     * Caso der erro na hora de pegar transformar o IP retorna o erro
     */
    if (temp == NULL) {
        return GET_NETMASK_FAILED;
    }

    strcpy(iface->interface_netmask, (const char *) temp);

    /*
     * Se chegar aqui os dados foram 
     * recuperados com sucesso
     */
    return GET_NETMASK_SUCCESS;
}

int get_interface_broadcast(Interface* iface, struct sockaddr_in * broadcast) {
    /*
     * Agora transforma o Broadcast que está em bytes
     * para IP legível com numeros e pontos
     */
    char * temp = inet_ntoa(broadcast->sin_addr);

    /*
     * Caso der erro na hora de pegar transformar o IP retorna o erro
     */
    if (temp == NULL)
        return GET_BROADCAST_FAILED;

    strcpy(iface->interface_broadcast, (const char *) temp);

    /*
     * Se chegar aqui os dados foram 
     * recuperados com sucesso
     */
    return GET_BROADCAST_SUCCESS;
}

int get_interface_mac_addr(Interface* iface) {
    char arquivo[100]; //Arquivo que sera aberto
    /*
     * Completa o nome do arquivo, com o 
     * nome da interface
     */
    snprintf(arquivo, sizeof (arquivo), "/sys/class/net/%s/address", iface->interface_name);

    /*
     * System call para abrir o arquivo
     */
    int fd; //Variavel de file descriptor

    fd = open((const char *) arquivo, O_RDONLY, 0);

    /*
     * Declaração e alocação do buffer para pegar
     * os dados do arquivo
     */
    char buf[100] = "";

    /*
     * Faz a leitura e joga no buffer
     */
    if (read(fd, buf, sizeof (buf)) == 0)
        return GET_MAC_ADDR_FAILED;

    /*
     * Agora, faz a copia do que tem no buffer 
     * para a struct
     */
    strncpy(iface->interface_mac_addr, buf, strlen(buf) - 1);

    /*
     * Fecha o arquivo
     */
    close(fd);

    /*
     * Retorna Sucesso
     */
    return GET_MAC_ADDR_SUCCESS;
}

int save_interfaces_info(DBConnection * conn, Interface* iface) {
    Query query;
    /*
     * Cria a variavel da sql
     */
    char sql[300] = "";
    
    /** Deleta os dados da tabela */
    strcpy(query.sql, "DELETE FROM interfaces;");
    execute_query(conn, &query);
    /*
     * Roda toda a lista ligada para salvar os dados
     */

    while (iface->next_interface != NULL) {
        /*
         * Formata a variavel da sql
         */
        snprintf(sql, sizeof (sql),
                "INSERT INTO interfaces (nome,endereco_ipv4,endereco_ipv6, netmask,broadcast,mac_address, ativa) " \
                " VALUES ('%s','%s','%s','%s','%s',upper('%s'),'%s')",
                iface->interface_name,
                iface->interface_addr_ipv4,
                iface->interface_addr_ipv6,
                iface->interface_netmask,
                iface->interface_broadcast,
                iface->interface_mac_addr,
                iface->interface_active == INTERFACE_ACTIVE ? "true" : "false");
        strcpy(query.sql, sql);
        execute_query(conn, &query);

        iface = iface->next_interface;

    }
    /** Libera o PGResult */
    clear_query(&query);

    return SAVE_INFO_IFACES_SUCCESS;
}

void free_interfaces(Interface* iface) {
    Interface * aux;

    while (iface->next_interface != NULL) {
        aux = iface;
        iface = iface->next_interface;
        free(aux);
    }
}
