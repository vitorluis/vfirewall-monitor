/* 
 * File:   interfaces.h
 * Author: vitor
 *
 * Created on September 16, 2013, 9:09 PM
 */

#ifndef INTERFACES_H
#define	INTERFACES_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "defines.h"
#include "database.h"
#include "logger.h"

//Struct que contera a informação das interfaces

typedef struct iface {
    char interface_name[10];
    char interface_addr_ipv4[20];
    char interface_addr_ipv6[40];
    char interface_netmask[20];
    char interface_broadcast[20];
    char interface_mac_addr[50];
    int interface_active;
    struct iface * next_interface;

} Interface;

//Função principal que ira pegar os dados da interface
int get_interfaces_info(Interface * iface);

int get_interface_ifaddr(Interface * iface, struct sockaddr_in * ip);

int get_interface_netmask(Interface * iface, struct sockaddr_in * netmask);

int get_interface_broadcast(Interface * iface, struct sockaddr_in * broadcast);

int get_interface_mac_addr(Interface * iface);

int save_interfaces_info(DBConnection * conn, Interface * iface);

void free_interfaces(Interface * iface);

#endif	/* INTERFACES_H */
