#include "memory.h"

int get_memory_info(Memory* mem) {
    /*
     * Chama a função sysinfo para
     * pegar os dados da memoria
     */
    struct sysinfo info;

    if (sysinfo(&info) == -1) {
        /** Insere nos logs */
        insert_log(ERROR, LOG_SYSINFO, "Falha na syscall sysinfo() para dados da RAM - memory.c");

        /** Retorna falha */
        return GET_MEM_INFO_FAILED;
    }

    /*
     * Se chegar aqui, a execução do sysinfo
     * foi feita com sucesso
     */

    /*
     * Total de memoria
     */
    mem->total_memory = info.totalram;

    /*
     * Total de memoria livre
     */
    mem->free_memory = info.freeram;

    /*
     * Total de memoria utilizada
     */
    mem->used_memory = mem->total_memory - mem->free_memory;

    /*
     * Com as informações capturadas
     * retorna sucesso
     */

    return GET_MEM_INFO_SUCCESS;
}

int get_swap_info(Swap* swap) {
    /*
     * Chama a função sysinfo para
     * pegar os dados do swap
     */
    struct sysinfo info;

    if (sysinfo(&info) == -1) {
        /** Insere nos logs */
        insert_log(ERROR, LOG_SYSINFO, "Falha na syscall sysinfo() para dados da swap - memory.c");

        /** Retorna a falha */
        return GET_SWAP_INFO_FAILED;
    }

    /*
     * Se chegar aqui, a execução do sysinfo
     * foi feita com sucesso
     */

    /*
     * Total de swap
     */
    swap->total_swap = info.totalswap;

    /*
     * Total de swap livre
     */
    swap->free_swap = info.freeswap;

    /*
     * Total de swap utilizada
     */
    swap->used_swap = swap->total_swap - swap->free_swap;

    /*
     * Com as informações capturadas
     * retorna sucesso
     */

    return GET_SWAP_INFO_SUCCESS;
}

int save_memory_info(DBConnection * conn, Memory* mem, Swap * swap) {
    /*
     * Cria a sql
     * e aloca a mesma
     */
    char sql[150];

    /*
     * Cria a query
     * e aloca a mesma
     */

    Query query;
    
    /*
     * Limpa os dados para atualizar
     */
    strcpy(query.sql, "DELETE FROM memoria");
    execute_query(conn, &query);

    /*
     * Salva as informações da memória RAM
     */

    snprintf(sql, sizeof(sql),
            "INSERT INTO memoria (tipo,total_size,total_free,total_used) VALUES " \
            "('%s',%lu,%lu,%lu);",
            "RAM",
            mem->total_memory,
            mem->free_memory,
            mem->used_memory
            );

    strcpy(query.sql, sql);

    /*
     * Executa a query
     */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Limpa o PGResult */
        clear_query(&query);

        /** Insere nos logs */
        insert_log(ERROR, LOG_SYSINFO, "Falha ao salvar dados da memória RAM - memory.c");

        /** Retorna a falha */
        return SAVE_MEMORY_INFO_FAILED;
    }

    /*
     * Limpa o PGResult 
     */
    clear_query(&query);

    /*
     * Agora salva a informação do swap
     */
    snprintf(sql, sizeof(sql),
            "INSERT INTO memoria (tipo,total_size,total_free,total_used) VALUES " \
            "('%s',%lu,%lu,%lu);",
            "SWAP",
            swap->total_swap,
            swap->free_swap,
            swap->used_swap
            );
    strcpy(query.sql, sql);
    /*
     * Executa a query
     */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Limpa o PGresult */
        clear_query(&query);
        
        /** Insere nos logs */
        insert_log(ERROR, LOG_SYSINFO, "Falha ao salvar dados da Swap - memory.c");

        /** Retorna a falha */
        return SAVE_MEMORY_INFO_FAILED;
    }

    /** Limpa o PGResult*/
    clear_query(&query);
    return SAVE_MEMORY_INFO_SUCCESS;
}