/* 
 * File:   memory.h
 * Author: vitor
 *
 * Created on September 22, 2013, 9:26 PM
 */

#ifndef MEMORY_H
#define	MEMORY_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include "defines.h"
#include "database.h"
#include "logger.h"

typedef struct memory {
    unsigned long total_memory;
    unsigned long free_memory;
    unsigned long used_memory;
} Memory;

typedef struct swap {
    unsigned long total_swap;
    unsigned long free_swap;
    unsigned long used_swap;
} Swap;

int get_memory_info(Memory * mem);

int get_swap_info(Swap * swap);

int save_memory_info(DBConnection * conn, Memory * mem, Swap * swap);
#endif	/* MEMORY_H */

