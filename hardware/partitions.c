#include "partitions.h"

int get_partitions_info(Partition* part) {
    /*
     * Função principal que irá chamar
     * as outras funções para pegar 
     * os dados de todas as partições do sistema.
     */

    /** Compila a regex */
    regex_t * regexp;
    regexp = malloc(sizeof (regex_t));

    if (compile_regex(regexp) == REGEX_COMPILE_FAILED) {
        /** Insere no log */
        insert_log(ERROR, LOG_SYSINFO, "Falha na compilação do Regex - partitions.c");

        /** Retorna a falha */
        return GET_PARTITION_INFO_FAILED;
    }

    /** Faz o parse do mtab e cria a lista de partições */
    if (parse_mtab(regexp, part) == PARSE_MTAB_FAILED) {
        /** Insere no log */
        insert_log(ERROR, LOG_SYSINFO, "Falha ao fazer o parse do arquivo /etc/mtab - partitions.c");

        /** Retorna a falha */
        return GET_PARTITION_INFO_FAILED;
    }

    if (get_partition_space(part) == GET_PARTITION_SPACE_FAILED) {
        /** Insere nos logs */
        insert_log(WARNING, LOG_SYSINFO, "Falha ao recuperar os dados de espaço total, utilizado e livre - partitions.c");

        /** Retorna a falha */
        return GET_PARTITION_INFO_FAILED;
    }

    /** Libera memória */
    free(regexp);

    return GET_PARTITION_INFO_SUCCESS;
}

int get_partition_space(Partition* part) {
    /** Variavel auxiliar */
    Partition * aux;

    /** Struct statvfs para pegar os dados */
    struct statvfs * info;

    /** Faz o loop para pegar os dados de cada partição */
    for (aux = part; aux->next_partition != NULL; aux = aux->next_partition) {
        /** Aloca a struct statvfs */
        info = malloc(sizeof (struct statvfs));

        /** Tenta pegar os dados */
        if (statvfs((const char *) aux->mnt_point, info)) {
            insert_log(ERROR, LOG_SYSINFO, "Chamada ao statvfs() falhou - partitions.c");
            return GET_PARTITION_SPACE_FAILED;
        }


        /*
         * Se chegar aqui, conseguiu pegar os dados 
         * Agora atribui os valores
         */
        aux->total_size = info->f_blocks * info->f_bsize;
        aux->total_free = info->f_bavail * info->f_bsize;
        aux->total_used = aux->total_size - aux->total_free;

        /** Libera a struct statvfs */
        free(info);
    }

    /** Se chegou aqui, conseguiu pegar as informações de todas as partições */
    return GET_PARTITION_SPACE_SUCCESS;
}

int parse_mtab(regex_t * regex, Partition * part) {
    /** Variavel auxiliar */
    Partition * aux;
    aux = part;

    struct mntent* ent;
    ent = malloc(sizeof (struct mntent));

    /** Ponteiro para o arquivo */
    FILE * file;

    /** Faz o parse do arquivo */
    file = setmntent("/etc/mtab", "r");

    /** Se o ponteiro for nulo, deu pau */
    if (file == NULL)
        return PARSE_MTAB_FAILED;

    /** Loop para rodar as linhas do arquivo /etc/mtab */
    while ((ent = getmntent(file)) != NULL) {

        /** Tenta aplicar a regex */

        if (regexec(regex, (const char *) ent->mnt_fsname, 0, NULL, 0) == REG_NOMATCH)
            continue;

        /** Se chegar, a regex bateu */

        /** Atribui os valores a struct de partições */
        strcpy(aux->dev_path, ent->mnt_fsname);
        strcpy(aux->mnt_point, ent->mnt_dir);
        strcpy(aux->fs_type, ent->mnt_type);

        /** Aloca a proxima struct */
        aux->next_partition = malloc(sizeof (Partition));
        aux = aux->next_partition;
        aux->next_partition = NULL;
    }

    /** Libera memória */
    endmntent(file);
    regfree(regex);
    /** Se chegar aqui, conseguiu fazer o parse  */
    return PARSE_MTAB_SUCCESS;
}

int compile_regex(regex_t * regex) {

    /** Tenta compilar a regex */
    if (regcomp(regex, "^/dev", REG_ICASE & REG_EXTENDED))
        return REGEX_COMPILE_FAILED;

    /** Se chegar aqui, a regex foi compilada com sucesso */
    return REGEX_COMPILE_SUCCESS;

}

int save_partitions_info(DBConnection * conn, Partition* part) {
    /** Função para salvar os dados das partições */
    /*
     * Cria e aloca a variavel da sql
     */
    char query_sql[300];
    /*
     * Roda toda a lista ligada para salvar os dados
     */
    Partition * aux;

    Query query;
    /** Deleta os dados existentes na tabela */
    strcpy(query_sql, "DELETE FROM particoes");
    strcpy(query.sql, query_sql);
    execute_query(conn, &query);

    for (aux = part; aux->next_partition != NULL; aux = aux->next_partition) {

        /*
         * Formata a variavel da sql
         */
        snprintf(query_sql, sizeof(query_sql),
                "INSERT INTO particoes (dev_path,mount_point,fs_type,total_size,total_free,total_used) " \
                " VALUES ('%s','%s','%s',%lu, %lu, %lu);",
                aux->dev_path,
                aux->mnt_point,
                aux->fs_type,
                aux->total_size,
                aux->total_free,
                aux->total_used);

        strcpy(query.sql, query_sql);

        execute_query(conn, &query);
    }
    
    /** Limpa memória da query */
    clear_query(&query);
    
    return SAVE_PARTITION_INFO_SUCCESS;
}

void free_partitions(Partition* part) {
    Partition * aux;

    while (part != NULL) {
        aux = part;
        part = part->next_partition;
        free(aux);
    }
}