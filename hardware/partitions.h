/* 
 * File:   partitions.h
 * Author: vitor
 *
 * Created on September 29, 2013, 7:09 PM
 */

#ifndef PARTITIONS_H
#define	PARTITIONS_H
#include <stdio.h>
#include <stdlib.h>
#include <mntent.h>
#include <sys/types.h>
#include <regex.h>
#include <sys/statvfs.h>
#include <unistd.h>
#include <string.h>
#include "defines.h"
#include "database.h"
#include "logger.h"

typedef struct partition {
    char dev_path[20];
    char mnt_point[50];
    char fs_type[20];
    unsigned long total_size;
    unsigned long total_free;
    unsigned long total_used;
    struct partition * next_partition;
} Partition;

int get_partitions_info(Partition * part);

int save_partitions_info(DBConnection * conn, Partition * part);

int parse_mtab(regex_t * regex, Partition * part);

int compile_regex(regex_t * regex);

int get_partition_space(Partition * part);

void free_partitions(Partition * part);




#endif	/* PARTITIONS_H */

