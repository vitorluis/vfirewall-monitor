#include "sysinfo.h"

int get_cpu_info(CPU* cpu) {
    /** Função que vai pegar os dados da CPU */

    /** Executar comando na console e pegar a saída */
    FILE * fp;

    /** Model name */
    fp = popen("cat /proc/cpuinfo | grep 'model name' | head -1 | cut -d: -f2", "r");
    if (fp == NULL)
        return GET_CPU_INFO_FAILED;

    if (fgets(cpu->model_name, sizeof (cpu->model_name), fp) == 0)
        return GET_CPU_INFO_FAILED;

    fp = popen("cat /proc/cpuinfo | grep 'vendor_id' | head -1 | cut -d: -f2", "r");
    if (fp == NULL)
        return GET_CPU_INFO_FAILED;

    if (fgets(cpu->vendor_id, sizeof (cpu->vendor_id), fp) == 0)
        return GET_CPU_INFO_FAILED;

    fclose(fp);

    /** CPU Freq */

    fp = popen("cat /proc/cpuinfo | grep 'MHz' | head -1 | cut -d: -f2", "r");
    if (fp == NULL)
        return GET_CPU_INFO_FAILED;

    if (fgets(cpu->cpu_freq, sizeof (cpu->cpu_freq), fp) == 0)
        return GET_CPU_INFO_FAILED;
    fclose(fp);

    /** Numero de Cores */
    fp = popen("cat /proc/cpuinfo | grep 'model name' | wc -l", "r");
    if (fp == NULL)
        return GET_CPU_INFO_FAILED;

    char buff[100];

    if (fgets(buff, sizeof (buff), fp) == 0)
        return GET_CPU_INFO_FAILED;

    cpu->n_cores = atoi(buff);
    fclose(fp);

    /** Retorna sucesso */
    return GET_CPU_INFO_SUCCESS;
}

int get_system_info(Sysinfo * sys) {
    /** Recuperação de dados do sistema */

    /** struct uname */
    struct utsname sysinfo;

    /** Tenta pegar os dados, caso contrario retorna falha */
    if (uname(&sysinfo) == -1)
        return GET_SYSINFO_FAILED;

    /** Atribui os valores na struct sysinfo */
    /** Arquitetura do sistema */
    strcpy(sys->arch, sysinfo.machine);

    /** Versão do Kernel */
    strcpy(sys->kernel_version, sysinfo.release);

    /** Hostname */
    strcpy(sys->hostname, sysinfo.nodename);

    /** Retorna sucesso */
    return GET_SYSINFO_SUCCESS;
}

int save_cpu_info(DBConnection* conn, CPU* cpu) {
    /** Salvar os dados da CPU */
    Query query;

    char query_sql[300];

    /*
     * Formata a variavel da sql
     */
    /** Modelo da CPU */
    snprintf(query_sql, sizeof (query_sql),
            "INSERT INTO sistema (nome,valor) VALUES ('%s','%s');",
            "CPU_MODEL_NAME",
            cpu->model_name
            );
    strcpy(query.sql, query_sql);

    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED)
        return SAVE_CPU_INFO_FAILED;

    /** Fabricante da CPU */
    snprintf(query_sql, sizeof (query_sql),
            "INSERT INTO sistema (nome,valor) VALUES ('%s','%s');",
            "CPU_VENDOR",
            cpu->vendor_id
            );

    strcpy(query.sql, query_sql);

    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED)
        return SAVE_CPU_INFO_FAILED;

    /** Frequencia da CPU */
    snprintf(query_sql, sizeof (query_sql),
            "INSERT INTO sistema (nome,valor) VALUES ('%s','%s');",
            "CPU_FREQ",
            cpu->cpu_freq
            );

    strcpy(query.sql, query_sql);

    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED)
        return SAVE_CPU_INFO_FAILED;

    /** Numeros de Cores */
    snprintf(query_sql, sizeof (query_sql),
            "INSERT INTO sistema (nome,valor) VALUES ('%s','%d');",
            "CPU_N_CORES",
            cpu->n_cores
            );

    strcpy(query.sql, query_sql);

    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED)
        return SAVE_CPU_INFO_FAILED;

    /** Limpa memória da query */
    clear_query(&query);

    /** Chegando aqui, retorna sucesso */
    return SAVE_CPU_INFO_SUCCESS;
}

int save_system_info(DBConnection* conn, Sysinfo * sys) {
    /** Salva os dados do sistema */
    Query query;

    /** Apaga tudo na tabela sistema */
    strcpy(query.sql, "DELETE FROM sistema");

    execute_query(conn, &query);

    /*
     * Formata a variavel da sql
     */
    /** Hostname */
    snprintf(query.sql, sizeof (query.sql),
            "INSERT INTO sistema (nome,valor) VALUES ('%s','%s');",
            "HOSTNAME",
            sys->hostname
            );

    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED)
        return SAVE_SYSINFO_FAILED;

    /** Arquitetura */
    snprintf(query.sql, sizeof (query.sql),
            "INSERT INTO sistema (nome,valor) VALUES ('%s','%s');",
            "ARCH",
            sys->arch
            );

    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED)
        return SAVE_SYSINFO_FAILED;

    /** Versão do Kernel */
    snprintf(query.sql, sizeof (query.sql),
            "INSERT INTO sistema (nome,valor) VALUES ('%s','%s');",
            "KERNEL_VERSION",
            sys->kernel_version
            );

    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED)
        return SAVE_SYSINFO_FAILED;

    /** Limpa memória da query */
    clear_query(&query);

    /** Retorna o sucesso */
    return SAVE_SYSINFO_SUCCESS;

}