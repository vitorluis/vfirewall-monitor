/* 
 * File:   sysinfo.h
 * Author: vitor
 *
 * Created on September 30, 2013, 9:53 PM
 */

#ifndef SYSINFO_H
#define	SYSINFO_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/utsname.h>
#include <regex.h>
#include "defines.h"
#include "database.h"
#include "logger.h"

typedef struct {
    char model_name[100];
    char vendor_id[30];
    char cpu_freq[30];
    unsigned int n_cores;
} CPU;

typedef struct {
    char arch[10];
    char kernel_version[30];
    char hostname[20];
} Sysinfo;

int get_cpu_info(CPU * cpu);

int get_system_info(Sysinfo * sys);

int save_cpu_info(DBConnection * conn, CPU * cpu);

int save_system_info(DBConnection * conn, Sysinfo * sys);

#endif	/* SYSINFO_H */

