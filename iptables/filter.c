#include "filter.h"
#include "match.h"
config_t conf;

int init_filter(DBConnection* conn) {
    /** Tenta abrir o arquivo de regras */
    int fd = open_filter_rules_file();

    if (fd == OPEN_RULES_FILE_FAILED) {
        /** Retorna a falha */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /*
     * Se o arquivo abriu gera as regras 
     * primeiramente, gera as politicas padrões
     */
    if (generate_filter_policy(fd, conn) == GENERATE_FILTER_POLICY_FAILED) {
        /** Retorna a falha */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /*
     * Chegando aqui, conseguiu gerar as politicas padrões 
     * Gera do input
     */
    if (generate_filter_input_rules(fd, conn) == GENERATE_FILTER_RULES_FAILED) {
        /** Retorna a falha */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /** Gera do output */
    if (generate_filter_output_rules(fd, conn) == GENERATE_FILTER_RULES_FAILED) {
        /** Retorna a falha */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /** Gera do forward */
    if (generate_filter_forward_rules(fd, conn) == GENERATE_FILTER_RULES_FAILED) {
        /** Retorna a falha */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /** Concatena um commit */
    if (write_filter_rules_file(fd, "COMMIT\n") == WRITE_RULE_FILE_FAILED) {
        /** Não conseguiu gravar no arquivo, retorna o erro */
        return GENERATE_FILTER_RULES_FAILED;
    }
    
     /** fecha o arquivo */
    close_filter_rules_file(fd);

    /** Chegando aqui, retorna sucesso */
    return GENERATE_FILTER_RULES_SUCCESS;
}

int open_filter_rules_file() {
    /** Nome do arquivo de regras */
    const char * filename = get_config_str(&conf, "filter_rules_file");

    /** Variavel do File descriptor */
    int fd;

    /** Verifica se o arquivo existe */
    if (access(filename, F_OK) != 0) {
        /*
         * Se entrar aqui, o arquivo não existe 
         * Então cria
         */
        fd = creat(filename, O_WRONLY | O_APPEND);

        /** Verifica se conseguiu criar o arquivo */
        if (fd == OPEN_RULES_FILE_FAILED) {
            /** Insere nos logs */
            insert_log(FATAL, LOG_FIREWALL, "Não foi possível criar o arquivo de regras - filter.c");

            /** Retorna falha */
            return OPEN_RULES_FILE_FAILED;
        }

        /** Seta as permissões */
        chmod(filename, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    } else {
        /** Se entrar aqui, o arquivo existe, só abre */
        fd = open(filename, O_WRONLY | O_TRUNC);

        /** Verifica se conseguiu abrir o arquivo */
        if (fd == OPEN_RULES_FILE_FAILED) {
            /** Insere nos logs */
            insert_log(FATAL, LOG_FIREWALL, "Não foi possível abrir o arquivo de regras - filter.c");

            /** Retorna falha */
            return OPEN_RULES_FILE_FAILED;
        }
    }

    /** Retorna o file descriptor ou o erro */
    return fd;
}

int write_filter_rules_file(int fd, const char* content) {
    /** Faz a gravação no arquivo */
    if (write(fd, content, strlen(content)) == -1) {
        /** Insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Não foi possível gravar no arquivo de rules - filter.c");
        printf("%s\n", strerror(errno));
        /** Retorna a falha */
        return WRITE_RULE_FILE_FAILED;
    }
    /** Se chegar aqui, gravou com sucesso */
    return WRITE_RULE_FILE_SUCCESS;
}

void close_filter_rules_file(int fd) {
    close(fd);
}

int generate_filter_policy(int fd, DBConnection * conn) {
    /**
     * 
     * 
     * Gerar um arquivo para fazer o restore
     * Com o IPTables restore
     * o Arquivo tem o seguinte formato.
     * 
     
     *filter -> Table (O Asterisco faz parte do arquivo)
    :INPUT ACCEPT [0:0] - :CHAIN POLICYDEFAULT [0:0]
    :FORWARD ACCEPT [0:0] - :CHAIN POLICYDEFAULT [0:0]
    :OUTPUT ACCEPT [6:357] - :CHAIN POLICYDEFAULT [0:0]
    -A INPUT -s 10.10.10.102/32 -j ACCEPT - RULES
    -A INPUT -s 10.10.10.103/32 -j ACCEPT - RULES
    COMMIT - Commita as regras

     * Será necessário montar esse arquivo com as regras
     * E depois chamar o iptables-restore
     */

    /*
     * Agora pela as politicas padrão do banco e
     * insere as politicas para cada chain da filter 
     */
    /** Declara as vars necessárias */
    Row * row;
    Query query;

    /** Copia a query sql a ser executada */
    strcpy(query.sql, "SELECT chain,policy FROM firewall_policy WHERE tabela = 'FILTER'");

    /** Faz a execução */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Insere nos logs */
        insert_log(ERROR, LOG_FIREWALL, "Não foi possível recuperar as regras padrões - filter.c");

        /** Limpa a query */
        clear_query(&query);

        /** Retorna o erro */
        return GENERATE_FILTER_POLICY_FAILED;
    }

    /** Variavel que guardará a linha a ser escrita no arquivo */
    char policy[30];

    /** Verifica se a linha esta nula */
    row = fetch(&query);

    if (row == NULL) {
        /** insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Nenhuma politica padrão associada a tabela filter - filter.c");

        /** Retorna falha */
        return GENERATE_FILTER_POLICY_FAILED;
    }

    /** Se chegar aqui existe alguma regra na tabela */

    if (write_filter_rules_file(fd, "*filter\n") == WRITE_RULE_FILE_FAILED) {
        /** Insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Falha ao gravar no arquivo referente a tabela filter - filter.c");

        /** Retorna a falha */
        return GENERATE_FILTER_POLICY_FAILED;
    }


    /** Faz o loop para passar pelas chains de INPUT, FORWARD e OUTPUT*/
    for (; row->next_line != NULL; row = row->next_line) {
        /** Monta a linha */
        snprintf(policy, sizeof (policy), ":%s %s [0:0]\n", row->cell[0], row->cell[1]);

        /** Grava no arquivo */
        if (write_filter_rules_file(fd, policy) == WRITE_RULE_FILE_FAILED) {
            /** Insere nos logs */
            insert_log(FATAL, LOG_FIREWALL, "Falha na gravação no arquivo, politica da chain INPUT - filter.c");

            /** Retorna a falha */
            return GENERATE_FILTER_POLICY_FAILED;
        }
    }

    /** Libera memória */
    free_row(row);

    /** Se chegar aqui, retorna sucesso */
    return GENERATE_FILTER_POLICY_SUCCESS;
}

int generate_filter_input_rules(int fd, DBConnection* conn) {
    /*
     * Recupera as regras do banco de dados 
     * Cria um objeto do tipo query
     */
    Query query;

    bool have_proto = false;

    /*
     * Cria a sql 
     * Seleciona as regras por tabela e chain
     */
    strcpy(query.sql, "SELECT rule_index, in_iface, proto, source_addr, source_port, dest_addr, " \
                      "dest_port, id, action FROM firewall_filter_rules WHERE ativa = true and chain = 'INPUT';");

    /** Executa a query */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Se entrar, insere no log e retorna erro */
        insert_log(FATAL, LOG_FIREWALL, "Falha na leitura das regras de firewall (filter/INPUT) - filter.c");

        /** Libera memória */
        clear_query(&query);

        /** Retorna erro */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /** Se chegar aqui, faz o fetch das linhas do banco */
    Row * row;
    row = fetch(&query);

    /** Verifica se as linhas não estão null */
    if (row == NULL) {
        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Nenhuma regra da tabela filter foi lida do banco de dados (filter/INPUT) - filter.c");

        /** Retorna NULL */
        return NO_RULES;
    }
    /** Declara um buffer para a linha */
    char linha[300];
    char buff[50];
    char * match;

    /** Se  as linhas não estiverem nulas, faz um loop para percorre-las */
    while (row->next_line != NULL) {
        /*
         * Começa pela posição da regra 
         * Faz o append, ou insere em alguma posição?
         */
        if (atoi(row->cell[0]) == -1) {
            /** Se é -1, faz o append */
            strcpy(linha, "-A INPUT ");
        } else {
            /** Se não for, insere na posição desejada */
            snprintf(buff, sizeof (buff), "-I INPUT %s ", row->cell[0]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem In_iface */
        if (strcmp(row->cell[1], "") != 0) {
            /** Se entrar aqui tem, então coloca na regra */
            snprintf(buff, sizeof (buff), "--in-interface %s ", row->cell[1]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica agora se tem o protocolo */
        if (strcmp(row->cell[2], "") != 0) {
            /** Se entrar aqui, tem o protocolo, então colcoca na regra*/
            snprintf(buff, sizeof (buff), "--proto %s ", row->cell[2]);

            /** Concatena na linha */
            strcat(linha, buff);

            /** Assinala have_proto como true */
            have_proto = true;
        }

        /** Verifica se tem o src_addr */
        if (strcmp(row->cell[3], "") != 0) {
            /** Se entrar aqui tem */
            snprintf(buff, sizeof (buff), "--source %s ", row->cell[3]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * Verifica se tem o src_port
         * Só tem se existir o proto
         */
        if (strcmp(row->cell[4], "") != 0 && have_proto == true) {
            /** Insere na regra */
            snprintf(buff, sizeof (buff), "--source-port %s ", row->cell[4]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem o dest_addr */
        if (strcmp(row->cell[5], "") != 0) {
            /** Se entrar aqui, tem o destination address */
            snprintf(buff, sizeof (buff), "--destination %s ", row->cell[5]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * O destination port funciona da mesma forma que o src port
         * Precisa ter o proto
         */
        if (strcmp(row->cell[6], "") != 0 && have_proto == true) {
            /** Se entrar aqui te, dport, e tem proto */
            snprintf(buff, sizeof (buff), "--destination-port %s ", row->cell[6]);

            /** Concatena a linha */
            strcat(linha, buff);
        }

        /** Gera o match, caso houver */

        match = generate_filter_match(atoi(row->cell[7]));
        if (match != NULL) {
            /** Concatena o match na linha */
            strcat(linha, match);

            /** Libera a variavel match */
            free(match);
        }



        /** Agora, vem o target */
        snprintf(buff, sizeof (buff), "-j %s ", row->cell[8]);

        /** Concatena na linha */
        strcat(linha, buff);

        /** No final da linha concatenamos um \n*/
        strcat(linha, "\n");

        /** Faz o write da regra */
        if (write_filter_rules_file(fd, linha) == WRITE_RULE_FILE_FAILED) {
            /** Não conseguiu gravar no arquivo, retorna o erro */
            return GENERATE_FILTER_RULES_FAILED;
        }

        /** Se chegar aqui, Vai para a proxima linha */
        row = row->next_line;
    }

    /** Limpa o result set do PG */
    clear_query(&query);

    /** Desaloca o espaço alocado por row */
    free_row(row);

    /** Retorna sucesso */
    return GENERATE_FILTER_RULES_SUCCESS;

}

int generate_filter_output_rules(int fd, DBConnection * conn) {
    /*
     * Recupera as regras do banco de dados 
     * Cria um objeto do tipo query
     */
    Query query;

    bool have_proto = false;

    /*
     * Cria a sql 
     * Seleciona as regras por tabela e chain
     */
    strcpy(query.sql, "SELECT rule_index, out_iface, proto, source_addr, source_port, dest_addr, " \
                      "dest_port, id, action FROM firewall_filter_rules WHERE ativa = true and chain = 'OUTPUT';");

    /** Executa a query */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Se entrar, insere no log e retorna erro */
        insert_log(FATAL, LOG_FIREWALL, "Falha na leitura das regras de firewall (filter/OUTPUT) - filter.c");

        /** Retorna erro */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /** Se chegar aqui, faz o fetch das linhas do banco */
    Row * row;
    row = fetch(&query);

    /** Verifica se as linhas não estão null */
    if (row == NULL) {
        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Nenhuma regra da tabela filter foi lida do banco de dados (filter/OUTPUT) - filter.c");

        /** Retorna NULL */
        return NO_RULES;
    }
    /** Declara um buffer para a linha */
    char linha[300];
    char buff[50];
    char * match;

    /** Se  as linhas não estiverem nulas, faz um loop para percorre-las */
    while (row->next_line != NULL) {
        /*
         * Começa pela posição da regra 
         * Faz o append, ou insere em alguma posição?
         */
        if (atoi(row->cell[0]) == -1) {
            /** Se é -1, faz o append */
            strcpy(linha, "-A OUTPUT ");
        } else {
            /** Se não for, insere na posição desejada */
            snprintf(buff, sizeof (buff), "-I OUTPUT %s ", row->cell[0]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem In_iface */
        if (strcmp(row->cell[1], "") != 0) {
            /** Se entrar aqui tem, então coloca na regra */
            snprintf(buff, sizeof (buff), "--out-interface %s ", row->cell[1]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica agora se tem o protocolo */
        if (strcmp(row->cell[2], "") != 0) {
            /** Se entrar aqui, tem o protocolo, então colcoca na regra*/
            snprintf(buff, sizeof (buff), "--proto %s ", row->cell[2]);

            /** Concatena na linha */
            strcat(linha, buff);

            /** Assinala have_proto como true */
            have_proto = true;
        }

        /** Verifica se tem o src_addr */
        if (strcmp(row->cell[3], "") != 0) {
            /** Se entrar aqui tem */
            snprintf(buff, sizeof (buff), "--source %s ", row->cell[3]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * Verifica se tem o src_port
         * Só tem se existir o proto
         */
        if (strcmp(row->cell[4], "") != 0 && have_proto == true) {
            /** Insere na regra */
            snprintf(buff, sizeof (buff), "--source-port %s ", row->cell[4]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem o dest_addr */
        if (strcmp(row->cell[5], "") != 0) {
            /** Se entrar aqui, tem o destination address */
            snprintf(buff, sizeof (buff), "--destination %s ", row->cell[5]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * O destination port funciona da mesma forma que o src port
         * Precisa ter o proto
         */
        if (strcmp(row->cell[6], "") != 0 && have_proto == true) {
            /** Se entrar aqui te, dport, e tem proto */
            snprintf(buff, sizeof (buff), "--destination-port %s ", row->cell[6]);

            /** Concatena a linha */
            strcat(linha, buff);
        }

        /** Gera o match da regra, caso houver */
        match = generate_filter_match(atoi(row->cell[7]));
        if (match != NULL) {
            /** Concatena o match na linha */
            strcat(linha, match);

            /** Libera a variavel do match */
            free(match);
        }

        /** Agora, vem o target */
        snprintf(buff, sizeof (buff), "-j %s ", row->cell[8]);

        /** Concatena na linha */
        strcat(linha, buff);

        /** No final da linha concatenamos um \n*/
        strcat(linha, "\n");

        /** Faz o write da regra */
        if (write_filter_rules_file(fd, linha) == WRITE_RULE_FILE_FAILED) {
            /** Não conseguiu gravar no arquivo, retorna o erro */
            return GENERATE_FILTER_RULES_FAILED;
        }

        /** Se chegar aqui, Vai para a proxima linha */
        row = row->next_line;
    }

    /** Limpa o result set do PG */
    clear_query(&query);

    /** Desaloca o espaço alocado por row */
    free_row(row);

    /** Retorna sucesso */
    return GENERATE_FILTER_RULES_SUCCESS;
}

int generate_filter_forward_rules(int fd, DBConnection * conn) {
    /*
     * Recupera as regras do banco de dados 
     * Cria um objeto do tipo query
     */
    Query query;

    bool have_proto = false;

    /*
     * Cria a sql 
     * Seleciona as regras por tabela e chain
     */
    strcpy(query.sql, "SELECT rule_index, in_iface, proto, source_addr, source_port, dest_addr, " \
                      "dest_port, id, action FROM firewall_filter_rules WHERE ativa = true and chain = 'FORWARD';");

    /** Executa a query */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Se entrar, insere no log e retorna erro */
        insert_log(FATAL, LOG_FIREWALL, "Falha na leitura das regras de firewall (filter/FORWARD) - filter.c");

        /** Retorna erro */
        return GENERATE_FILTER_RULES_FAILED;
    }

    /** Se chegar aqui, faz o fetch das linhas do banco */
    Row * row;
    row = fetch(&query);

    /** Verifica se as linhas não estão null */
    if (row == NULL) {
        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Nenhuma regra da tabela filter foi lida do banco de dados (filter/FORWARD) - filter.c");

        /** Retorna NULL */
        return NO_RULES;
    }
    /** Declara um buffer para a linha */
    char linha[300];
    char buff[50];
    char * match;

    /** Se  as linhas não estiverem nulas, faz um loop para percorre-las */
    while (row->next_line != NULL) {
        /*
         * Começa pela posição da regra 
         * Faz o append, ou insere em alguma posição?
         */
        if (atoi(row->cell[0]) == -1) {
            /** Se é -1, faz o append */
            strcpy(linha, "-A FORWARD ");
        } else {
            /** Se não for, insere na posição desejada */
            snprintf(buff, sizeof (buff), "-I FORWARD %s ", row->cell[0]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem In_iface */
        if (strcmp(row->cell[1], "") != 0) {
            /** Se entrar aqui tem, então coloca na regra */
            snprintf(buff, sizeof (buff), "--in-interface %s ", row->cell[1]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica agora se tem o protocolo */
        if (strcmp(row->cell[2], "") != 0) {
            /** Se entrar aqui, tem o protocolo, então colcoca na regra*/
            snprintf(buff, sizeof (buff), "--proto %s ", row->cell[2]);

            /** Concatena na linha */
            strcat(linha, buff);

            /** Assinala have_proto como true */
            have_proto = true;
        }

        /** Verifica se tem o src_addr */
        if (strcmp(row->cell[3], "") != 0) {
            /** Se entrar aqui tem */
            snprintf(buff, sizeof (buff), "--source %s ", row->cell[3]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * Verifica se tem o src_port
         * Só tem se existir o proto
         */
        if (strcmp(row->cell[4], "") != 0 && have_proto == true) {
            /** Insere na regra */
            snprintf(buff, sizeof (buff), "--source-port %s ", row->cell[4]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem o dest_addr */
        if (strcmp(row->cell[5], "") != 0) {
            /** Se entrar aqui, tem o destination address */
            snprintf(buff, sizeof (buff), "--destination %s ", row->cell[5]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * O destination port funciona da mesma forma que o src port
         * Precisa ter o proto
         */
        if (strcmp(row->cell[6], "") != 0 && have_proto == true) {
            /** Se entrar aqui te, dport, e tem proto */
            snprintf(buff, sizeof (buff), "--destination-port %s ", row->cell[6]);

            /** Concatena a linha */
            strcat(linha, buff);
        }

        /** Gera o match da regra, caso houver */
        match = generate_filter_match(atoi(row->cell[7]));
        if (match != NULL) {
            /** Concatena o match na linha */
            strcat(linha, match);

            /** Libera a variavel do match */
            free(match);
        }


        /** Agora, vem o target */
        snprintf(buff, sizeof (buff), "-j %s ", row->cell[9]);

        /** Concatena na linha */
        strcat(linha, buff);

        /** No final da linha concatenamos um \n*/
        strcat(linha, "\n");

        /** Faz o write da regra */
        if (write_filter_rules_file(fd, linha) == WRITE_RULE_FILE_FAILED) {
            /** Não conseguiu gravar no arquivo, retorna o erro */
            return GENERATE_FILTER_RULES_FAILED;
        }

        /** Se chegar aqui, Vai para a proxima linha */
        row = row->next_line;
    }

    /** Limpa o result set do PG */
    clear_query(&query);

    /** Desaloca o espaço alocado por row */
    free_row(row);

    /** Retorna sucesso */
    return GENERATE_FILTER_RULES_SUCCESS;
}