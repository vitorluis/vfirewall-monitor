/* 
 * File:   filter.h
 * Author: vfirewall
 *
 * Created on November 6, 2013, 10:57 AM
 */

#ifndef FILTER_H
#define	FILTER_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include "firewall.h"
#include "config_file.h"
#include "database.h"
#include "defines.h"
#include "logger.h"
#include "firewall.h"
#include "match.h"

int init_filter(DBConnection * conn);

int generate_filter_policy(int fd, DBConnection * conn);

int generate_filter_input_rules(int fd, DBConnection * conn);

int generate_filter_output_rules(int fd, DBConnection * conn);

int generate_filter_forward_rules(int fd, DBConnection * conn);

int open_filter_rules_file();

int write_filter_rules_file(int fd, const char * content);

void close_filter_rules_file(int fd);

#endif	/* FILTER_H */

