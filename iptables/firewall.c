#include "firewall.h"
/** Variavel global do arquivo de configuração */
config_t conf;

int init_firewall_rules(DBConnection * conn) {
    /** chama o generate */
    if (generate_rules(conn) == GENERATE_RULES_FAILED) {
        /** Retorna falha */
        return INIT_FIREWALL_FAILED;
    }

    /** Chegando aqui, tenta aplicar as regras */
    if (apply_rules() == APPLY_RULES_FAILED) {
        /** Retorna falha */
        return INIT_FIREWALL_FAILED;
    }

    /** chegando aqui, retorna sucesso */
    return INIT_FIREWALL_SUCCESS;
}

int generate_rules(DBConnection* conn) {
    /** Começa gerando as regras da tabela filter */
    if (init_filter(conn) == GENERATE_FILTER_RULES_FAILED) {
        /** Retorna falha */
        return GENERATE_RULES_FAILED;
    }

    /** Agora gera a regra da tabela mangle */
    if (init_mangle(conn) == GENERATE_MANGLE_RULES_FAILED) {
        /** Retorna falha */
        return GENERATE_RULES_FAILED;
    }

    /** Por ultimo, gera da tabela NAT */
    if (init_nat(conn) == GENERATE_NAT_RULES_FAILED) {
        /** Retorna falha */
        return GENERATE_RULES_FAILED;
    }

    /** Chegando aqui, retorna sucesso */
    return GENERATE_RULES_SUCCESS;
}

int apply_rules() {
    /** Le o path dos arquivos das regras */
    char filter_rules_file[100];
    char mangle_rules_file[100];
    char nat_rules_file[100];

    /*
     * Copia os paths */
    strcpy(filter_rules_file, get_config_str(&conf, "filter_rules_file"));
    strcpy(mangle_rules_file, get_config_str(&conf, "mangle_rules_file"));
    strcpy(nat_rules_file, get_config_str(&conf, "nat_rules_file"));

    /** Chama a função system para aplicar as regras */
    char buff[200];
    snprintf(buff, sizeof (buff), "/sbin/iptables-restore < %s", filter_rules_file);
    if (system(buff) != 0) {
        /*
         * Se entrar aqui, o iptables restore não foi executado com sucesso
         * Insere nos logs 
         */
        insert_log(FATAL, LOG_FIREWALL, "Não foi possivel aplicar as regras no iptables referente a tabela filter - firewall.c");

        /** Retorna a falha */
        return APPLY_RULES_FAILED;
    }

    snprintf(buff, sizeof (buff), "/sbin/iptables-restore < %s", mangle_rules_file);
    if (system(buff) != 0) {
        /*
         * Se entrar aqui, o iptables restore não foi executado com sucesso
         * Insere nos logs 
         */
        insert_log(FATAL, LOG_FIREWALL, "Não foi possivel aplicar as regras no iptables referente a tabela mangle - firewall.c");

        /** Retorna a falha */
        return APPLY_RULES_FAILED;
    }

    snprintf(buff, sizeof (buff), "/sbin/iptables-restore < %s", nat_rules_file);
    if (system(buff) != 0) {
        /*
         * Se entrar aqui, o iptables restore não foi executado com sucesso
         * Insere nos logs 
         */
        insert_log(FATAL, LOG_FIREWALL, "Não foi possivel aplicar as regras no iptables referente a tabela nat - firewall.c");

        /** Retorna a falha */
        return APPLY_RULES_FAILED;
    }

    /** Retorna sucesso */
    return APPLY_RULES_SUCCESS;
}

int flush_rules() {
    if (system("/sbin/iptables --flush") != 0) {
        return IPTABLES_FLUSH_FAILED;
    } else {
        return IPTABLES_FLUSH_SUCCESS;
    }
}

int load_rules_file(const char* file) {
    /** Tenta fazer o load do arquivo */
    char command[500];
    
    /** Copia o nome do arquivo */
    snprintf(command,sizeof(command), "/sbin/iptables-restore < %s", file);
    
    if (system(command) != 0) {
        /** Insere nos logs */
        char log[500];
        snprintf(log, sizeof(log), "Não foi possivel carregar o arquivo de regras %s - firewall.c", file);
        insert_log(WARNING, LOG_FIREWALL, log);
        
        /** retorna a falha */
        return LOAD_FILE_RULES_FAILED;
    }
    
    /** Se chegar aqui, conseguiu carregar*/
    return LOAD_FILE_RULES_SUCCESS;
}