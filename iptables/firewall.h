/* 
 * File:   firewall.h
 * Author: vfirewall
 *
 * Created on October 17, 2013, 8:42 PM
 */

#ifndef FIREWALL_H
#define	FIREWALL_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include "defines.h"
#include "database.h"
#include "logger.h"
#include "thread.h"
#include "config_file.h"
#include "filter.h"
#include "mangle.h"
#include "nat.h"

int init_firewall_rules(DBConnection * conn);

int generate_rules(DBConnection * conn);

int load_rules_file(const char * file);

int apply_rules();

int flush_rules();
#endif	/* EVENT_H */

