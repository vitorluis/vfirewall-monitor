/* 
 * File:   mangle.h
 * Author: vfirewall
 *
 * Created on December 9, 2013, 10:32 AM
 */

#ifndef MANGLE_H
#define	MANGLE_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include "firewall.h"
#include "config_file.h"
#include "database.h"
#include "defines.h"
#include "logger.h"
#include "firewall.h"
#include "match.h"

int init_mangle(DBConnection * conn);

int generate_mangle_policy(int fd, DBConnection * conn);

int generate_mangle_input_rules(int fd, DBConnection * conn);

int generate_mangle_output_rules(int fd, DBConnection * conn);

int generate_mangle_forward_rules(int fd, DBConnection * conn);

int generate_mangle_prerouting_rules(int fd, DBConnection * conn);

int generate_mangle_postrouting_rules(int fd, DBConnection * conn);

int open_mangle_rules_file();

int write_mangle_rules_file(int fd, const char * content);

void close_mangle_rules_file(int fd);

#endif	/* MANGLE_H */

