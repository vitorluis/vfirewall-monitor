#include "match.h"
DBConnection * conn;

char * generate_filter_match(int rule_id) {
    /** Declara as variaveis */
    Query q;
    Row * r;
    JSON * json;
    char * rule;
    char buff[50];

    /** Prepara a query */
    snprintf(q.sql,
            sizeof (q.sql),
            "select match_type, match_params from firewall_filter_match where rule_id = %d",
            rule_id);

    /** Faz a execução */
    if (execute_query(conn, &q) != QUERY_EXECUTE_SUCCESS) {
        /** registra nos logs que não fez o match */
        insert_log(FATAL, LOG_FIREWALL, "Falha na busca do match - match.c");

        /** Retorna nulo */
        return NULL;
    }

    /** Se chegar aqui, pega o resultado */
    r = fetch(&q);

    if (r == NULL) {
        /** nenhuma match lida, retorna NULL */
        return NULL;
    }
    /** Inicializa a rule especificando o match */
    rule = malloc(sizeof (char) * 200);
    snprintf(rule, 200, " --match %s ", r->cell[0]);

    /** Agora decodifica o JSON */
    json = json_decode(r->cell[1]);
    int i;
    for (i = 0; i < json->pos; i++) {
        /** Monta a regra */
        snprintf(buff, sizeof (buff), "--%s ", json->keys[i]);
        strcat(rule, buff);

        if (strcmp(json->keys[i], "string") == 0) {
            /** Se o match for string, deve ser entre aspas */
            snprintf(buff, sizeof (buff), "\"%s\" ", json->values[i]);
        } else {
            snprintf(buff, sizeof (buff), "%s ", json->values[i]);
        }
        /** Concatena o valor na rule */
        strcat(rule, buff);
    }
    /** Libera os espaços alocados */
    clear_query(&q);
    free_row(r);


    /** No final, retorna a rule no formato do iptables */
    return rule;
}

char * generate_mangle_match(int rule_id) {
    /** Declara as variaveis */
    Query q;
    Row * r;
    JSON * json;
    char * rule;
    char buff[50];

    /** Prepara a query */
    snprintf(q.sql,
            sizeof (q.sql),
            "select match_type, match_params from firewall_mangle_match where rule_id = %d",
            rule_id);

    /** Faz a execução */
    if (execute_query(conn, &q) != QUERY_EXECUTE_SUCCESS) {
        /** registra nos logs que não fez o match */
        insert_log(FATAL, LOG_FIREWALL, "Falha na busca do match - match.c");

        /** Retorna nulo */
        return NULL;
    }

    /** Se chegar aqui, pega o resultado */
    r = fetch(&q);

    if (r == NULL) {
        /** nenhuma match lida, retorna NULL */
        return NULL;
    }
    /** Inicializa a rule especificando o match */
    rule = malloc(sizeof (char) * 200);
    snprintf(rule, 200, " --match %s ", r->cell[0]);

    /** Agora decodifica o JSON */
    json = json_decode(r->cell[1]);
    int i;
    for (i = 0; i < json->pos; i++) {
        /** Monta a regra */
        snprintf(buff, sizeof (buff), "--%s ", json->keys[i]);
        strcat(rule, buff);

        if (strcmp(json->keys[i], "string") == 0) {
            /** Se o match for string, deve ser entre aspas */
            snprintf(buff, sizeof (buff), "\"%s\" ", json->values[i]);
        } else {
            snprintf(buff, sizeof (buff), "%s ", json->values[i]);
        }
        /** Concatena o valor na rule */
        strcat(rule, buff);
    }
    /** Libera os espaços alocados */
    clear_query(&q);
    free_row(r);

    /** No final, retorna a rule no formato do iptables */
    return rule;
}

char * generate_nat_match(int rule_id) {
    /** Declara as variaveis */
    Query q;
    Row * r;
    JSON * json;
    char * rule;
    char buff[50];

    /** Prepara a query */
    snprintf(q.sql,
            sizeof (q.sql),
            "select match_type, match_params from firewall_nat_match where rule_id = %d",
            rule_id);

    /** Faz a execução */
    if (execute_query(conn, &q) != QUERY_EXECUTE_SUCCESS) {
        /** registra nos logs que não fez o match */
        insert_log(FATAL, LOG_FIREWALL, "Falha na busca do match - match.c");

        /** Retorna nulo */
        return NULL;
    }

    /** Se chegar aqui, pega o resultado */
    r = fetch(&q);

    if (r == NULL) {
        /** nenhuma match lida, retorna NULL */
        return NULL;
    }

    /** Inicializa a rule especificando o match */
    rule = malloc(sizeof (char) * 200);
    snprintf(rule, 200, " --match %s ", r->cell[0]);

    /** Agora decodifica o JSON */
    json = json_decode(r->cell[1]);
    int i;
    for (i = 0; i < json->pos; i++) {
        /** Monta a regra */
        snprintf(buff, sizeof (buff), "--%s ", json->keys[i]);
        strcat(rule, buff);

        if (strcmp(json->keys[i], "string") == 0) {
            /** Se o match for string, deve ser entre aspas */
            snprintf(buff, sizeof (buff), " \"%s\" ", json->values[i]);
        } else {
            snprintf(buff, sizeof (buff), " %s ", json->values[i]);
        }
        /** Concatena o valor na rule */
        strcat(rule, buff);
    }

    /** Libera os espaços alocados */
    clear_query(&q);
    free_row(r);
    
    /** No final, retorna a rule no formato do iptables */
    return rule;
}
