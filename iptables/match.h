/* 
 * File:   match.h
 * Author: vitor
 *
 * Created on February 18, 2014, 5:14 PM
 */

#ifndef MATCH_H
#define	MATCH_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "database.h"
#include "json.h"
#include "logger.h"

/** Função que gerará o match das regras em relação a tabela filter */
char * generate_filter_match(int rule_id);

/** Função que gerará o match das regras em relação a tabela mangle */
char * generate_mangle_match(int rule_id);

/** Função que gerará o match das regras em relação a tabela nat */
char * generate_nat_match(int rule_id);

#endif	/* MATCH_H */

