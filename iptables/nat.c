#include "nat.h"

int init_nat(DBConnection* conn) {
    /** Tenta abrir o arquivo de regras */
    int fd = open_nat_rules_file();

    if (fd == OPEN_RULES_FILE_FAILED) {
        /** Retorna a falha */
        return GENERATE_NAT_RULES_FAILED;
    }

    /*
     * Se o arquivo abriu gera as regras 
     * primeiramente, gera as politicas padrões
     */
    if (generate_nat_policy(fd, conn) == GENERATE_NAT_POLICY_FAILED) {
        /** Retorna a falha */
        return GENERATE_NAT_RULES_FAILED;
    }

    /*
     * Chegando aqui, conseguiu gerar as politicas padrões 
     */

    /** Gera do output */
    if (generate_nat_output_rules(fd, conn) == GENERATE_NAT_RULES_FAILED) {
        /** Retorna a falha */
        return GENERATE_NAT_RULES_FAILED;
    }

    /** Gera do prerouting */
    if (generate_nat_prerouting_rules(fd, conn) == GENERATE_NAT_RULES_FAILED) {
        /** Retorna a falha */
        return GENERATE_NAT_RULES_FAILED;
    }

    /** Gera do postrouting */
    if (generate_nat_postrouting_rules(fd, conn) == GENERATE_NAT_RULES_FAILED) {
        /** Retorna a falha */
        return GENERATE_NAT_RULES_FAILED;
    }

    /** Coloca o commit no final */
    if (write_nat_rules_file(fd, "COMMIT\n") == WRITE_RULE_FILE_FAILED) {
        /** Não conseguiu gravar no arquivo, retorna o erro */
        return GENERATE_NAT_RULES_FAILED;
    }

    /** Se chegar aqui, gravou o commit, fecha o arquivo */
    close_nat_rules_file(fd);
    
    /** Chegando aqui, retorna sucesso */
    return GENERATE_NAT_RULES_SUCCESS;
}

int open_nat_rules_file() {
    /** Nome do arquivo de regras */
    const char * filename = get_config_str(&conf, "nat_rules_file");

    /** Variavel do File descriptor */
    int fd;

    /** Verifica se o arquivo existe */
    if (access(filename, F_OK) != 0) {
        /*
         * Se entrar aqui, o arquivo não existe 
         * Então cria
         */
        fd = creat(filename, O_WRONLY | O_APPEND);

        /** Verifica se conseguiu criar o arquivo */
        if (fd == OPEN_RULES_FILE_FAILED) {
            /** Insere nos logs */
            insert_log(FATAL, LOG_FIREWALL, "Não foi possível criar o arquivo de regras - nat.c");

            /** Retorna falha */
            return OPEN_RULES_FILE_FAILED;
        }

        /** Seta as permissões */
        chmod(filename, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    } else {
        /** Se entrar aqui, o arquivo existe, só abre */
        fd = open(filename, O_WRONLY | O_TRUNC);

        /** Verifica se conseguiu abrir o arquivo */
        if (fd == OPEN_RULES_FILE_FAILED) {
            /** Insere nos logs */
            insert_log(FATAL, LOG_FIREWALL, "Não foi possível abrir o arquivo de regras - nat.c");

            /** Retorna falha */
            return OPEN_RULES_FILE_FAILED;
        }
    }

    /** Retorna o file descriptor ou o erro */
    return fd;
}

int write_nat_rules_file(int fd, const char* content) {
    /** Faz a gravação no arquivo */
    if (write(fd, content, strlen(content)) == -1) {
        /** Insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Não foi possível gravar no arquivo de rules - nat.c");

        /** Retorna a falha */
        return WRITE_RULE_FILE_FAILED;
    }
    /** Se chegar aqui, gravou com sucesso */
    return WRITE_RULE_FILE_SUCCESS;
}

void close_nat_rules_file(int fd) {
    close(fd);
}

int generate_nat_policy(int fd, DBConnection * conn) {
    /*
     * Agora pela as politicas padrão do banco e
     * insere as politicas para cada chain da nat 
     */
    /** Declara as vars necessárias */
    Row * row;
    Query query;

    /** Copia a query sql a ser executada */
    strcpy(query.sql, "SELECT chain,policy FROM firewall_policy WHERE tabela = 'NAT'");

    /** Faz a execução */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Insere nos logs */
        insert_log(ERROR, LOG_FIREWALL, "Não foi possível recuperar as regras padrões - nat.c");

        /** Limpa a query */
        clear_query(&query);

        /** Retorna o erro */
        return GENERATE_NAT_POLICY_FAILED;
    }

    /** Variavel que guardará a linha a ser escrita no arquivo */
    char policy[30];

    /** Verifica se a linha esta nula */
    row = fetch(&query);

    if (row == NULL) {
        /** insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Nenhuma politica padrão associada a tabela nat - nat.c");

        /** Retorna falha */
        return NO_RULES;
    }
    /** Se chegar aqui existe alguma regra na tabela */

    if (write_nat_rules_file(fd, "*nat\n") == WRITE_RULE_FILE_FAILED) {
        /** Insere nos logs */
        insert_log(FATAL, LOG_FIREWALL, "Falha ao gravar no arquivo referente a tabela nat - nat.c");

        /** Retorna a falha */
        return GENERATE_NAT_POLICY_FAILED;
    }


    /** Faz o loop para passar pelas chains de INPUT, FORWARD e OUTPUT*/
    for (; row->next_line != NULL; row = row->next_line) {
        /** Monta a linha */
        snprintf(policy, sizeof (policy), ":%s %s [0:0]\n", row->cell[0], row->cell[1]);

        /** Grava no arquivo */
        if (write_nat_rules_file(fd, policy) == WRITE_RULE_FILE_FAILED) {
            /** Insere nos logs */
            insert_log(FATAL, LOG_FIREWALL, "Falha na gravação no arquivo, politica da chain INPUT - nat.c");

            /** Retorna a falha */
            return GENERATE_NAT_POLICY_FAILED;
        }
    }

    /** Libera memória */
    free_row(row);

    /** Se chegar aqui, retorna sucesso */
    return GENERATE_NAT_POLICY_SUCCESS;
}

int generate_nat_output_rules(int fd, DBConnection* conn) {
    /*
     * Recupera as regras do banco de dados 
     * Cria um objeto do tipo query
     */
    Query query;

    bool have_proto = false;

    /*
     * Cria a sql 
     * Seleciona as regras por tabela e chain
     */
    strcpy(query.sql, "SELECT rule_index, out_iface, proto, source_addr, source_port, dest_addr, " \
                      "dest_port, id, action FROM firewall_nat_rules WHERE ativa = true and chain = 'OUTPUT';");

    /** Executa a query */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Se entrar, insere no log e retorna erro */
        insert_log(FATAL, LOG_FIREWALL, "Falha na leitura das regras de firewall (nat/OUTPUT) - nat.c");

        /** Retorna erro */
        return GENERATE_NAT_RULES_FAILED;
    }

    /** Se chegar aqui, faz o fetch das linhas do banco */
    Row * row;
    row = fetch(&query);

    /** Verifica se as linhas não estão null */
    if (row == NULL) {
        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Nenhuma regra da tabela nat foi lida do banco de dados (nat/OUTPUT) - nat.c");

        /** Retorna NULL */
        return NO_RULES;
    }
    /** Declara um buffer para a linha */
    char linha[300];
    char buff[50];
    char * match;

    /** Se  as linhas não estiverem nulas, faz um loop para percorre-las */
    while (row->next_line != NULL) {
        /*
         * Começa pela posição da regra 
         * Faz o append, ou insere em alguma posição?
         */
        if (atoi(row->cell[0]) == -1) {
            /** Se é -1, faz o append */
            strcpy(linha, "-A OUTPUT ");
        } else {
            /** Se não for, insere na posição desejada */
            snprintf(buff, sizeof (buff), "-I OUTPUT %s ", row->cell[0]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem In_iface */
        if (strcmp(row->cell[1], "") != 0) {
            /** Se entrar aqui tem, então coloca na regra */
            snprintf(buff, sizeof (buff), "--out-interface %s ", row->cell[1]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica agora se tem o protocolo */
        if (strcmp(row->cell[2], "") != 0) {
            /** Se entrar aqui, tem o protocolo, então colcoca na regra*/
            snprintf(buff, sizeof (buff), "--proto %s ", row->cell[2]);

            /** Concatena na linha */
            strcat(linha, buff);

            /** Assinala have_proto como true */
            have_proto = true;
        }

        /** Verifica se tem o src_addr */
        if (strcmp(row->cell[3], "") != 0) {
            /** Se entrar aqui tem */
            snprintf(buff, sizeof (buff), "--source %s ", row->cell[3]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * Verifica se tem o src_port
         * Só tem se existir o proto
         */
        if (strcmp(row->cell[4], "") != 0 && have_proto == true) {
            /** Insere na regra */
            snprintf(buff, sizeof (buff), "--source-port %s ", row->cell[4]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem o dest_addr */
        if (strcmp(row->cell[5], "") != 0) {
            /** Se entrar aqui, tem o destination address */
            snprintf(buff, sizeof (buff), "--destination %s ", row->cell[5]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * O destination port funciona da mesma forma que o src port
         * Precisa ter o proto
         */
        if (strcmp(row->cell[6], "") != 0 && have_proto == true) {
            /** Se entrar aqui te, dport, e tem proto */
            snprintf(buff, sizeof (buff), "--destination-port %s ", row->cell[6]);

            /** Concatena a linha */
            strcat(linha, buff);
        }

        /** Gera o match da regra, caso houver */
        match = generate_nat_match(atoi(row->cell[7]));
        if (match != NULL) {
            /** Concatena o match na linha */
            strcat(linha, match);

            /** Libera a variavel do match */
            free(match);
        }

        /** Agora, vem o target */
        snprintf(buff, sizeof (buff), "-j %s ", row->cell[8]);

        /** Concatena na linha */
        strcat(linha, buff);

        /** No final da linha concatenamos um \n*/
        strcat(linha, "\n");

        /** Faz o write da regra */
        if (write_nat_rules_file(fd, linha) == WRITE_RULE_FILE_FAILED) {
            /** Não conseguiu gravar no arquivo, retorna o erro */
            return GENERATE_NAT_RULES_FAILED;
        }

        /** Se chegar aqui, Vai para a proxima linha */
        row = row->next_line;
    }

    /** Limpa o result set do PG */
    clear_query(&query);

    /** Desaloca o espaço alocado por row */
    free_row(row);

    /** Retorna sucesso */
    return GENERATE_NAT_RULES_SUCCESS;
}

int generate_nat_prerouting_rules(int fd, DBConnection* conn) {
    /*
     * Recupera as regras do banco de dados 
     * Cria um objeto do tipo query
     */
    Query query;

    bool have_proto = false;

    /*
     * Cria a sql 
     * Seleciona as regras por tabela e chain
     */
    strcpy(query.sql, "SELECT rule_index, in_iface, proto, source_addr, source_port, dest_addr, " \
                      "dest_port, id, action, redirect_ip, " \
                      "redirect_port FROM firewall_nat_rules WHERE ativa = true and chain = 'PREROUTING';");

    /** Executa a query */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Se entrar, insere no log e retorna erro */
        insert_log(FATAL, LOG_FIREWALL, "Falha na leitura das regras de firewall (nat/PREROUTING) - nat.c");

        /** Retorna erro */
        return GENERATE_NAT_RULES_FAILED;
    }

    /** Se chegar aqui, faz o fetch das linhas do banco */
    Row * row;
    row = fetch(&query);

    /** Verifica se as linhas não estão null */
    if (row == NULL) {
        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Nenhuma regra da tabela nat foi lida do banco de dados (nat/PREROUTING) - nat.c");

        /** Retorna NULL */
        return NO_RULES;
    }
    /** Declara um buffer para a linha */
    char linha[300];
    char buff[50];
    char * match;

    /** Se  as linhas não estiverem nulas, faz um loop para percorre-las */
    while (row->next_line != NULL) {
        /*
         * Começa pela posição da regra 
         * Faz o append, ou insere em alguma posição?
         */
        if (atoi(row->cell[0]) == -1) {
            /** Se é -1, faz o append */
            strcpy(linha, "-A PREROUTING ");
        } else {
            /** Se não for, insere na posição desejada */
            snprintf(buff, sizeof (buff), "-I PREROUTING %s ", row->cell[0]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem In_iface */
        if (strcmp(row->cell[1], "") != 0) {
            /** Se entrar aqui tem, então coloca na regra */
            snprintf(buff, sizeof (buff), "--in-interface %s ", row->cell[1]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica agora se tem o protocolo */
        if (strcmp(row->cell[2], "") != 0) {
            /** Se entrar aqui, tem o protocolo, então colcoca na regra*/
            snprintf(buff, sizeof (buff), "--proto %s ", row->cell[2]);

            /** Concatena na linha */
            strcat(linha, buff);

            /** Assinala have_proto como true */
            have_proto = true;
        }

        /** Verifica se tem o src_addr */
        if (strcmp(row->cell[3], "") != 0) {
            /** Se entrar aqui tem */
            snprintf(buff, sizeof (buff), "--source %s ", row->cell[3]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * Verifica se tem o src_port
         * Só tem se existir o proto
         */
        if (strcmp(row->cell[4], "") != 0 && have_proto == true) {
            /** Insere na regra */
            snprintf(buff, sizeof (buff), "--source-port %s ", row->cell[4]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem o dest_addr */
        if (strcmp(row->cell[5], "") != 0) {
            /** Se entrar aqui, tem o destination address */
            snprintf(buff, sizeof (buff), "--destination %s ", row->cell[5]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * O destination port funciona da mesma forma que o src port
         * Precisa ter o proto
         */
        if (strcmp(row->cell[6], "") != 0 && have_proto == true) {
            /** Se entrar aqui te, dport, e tem proto */
            snprintf(buff, sizeof (buff), "--destination-port %s ", row->cell[6]);

            /** Concatena a linha */
            strcat(linha, buff);
        }

        /** Gera o match da regra, caso houver */
        match = generate_nat_match(atoi(row->cell[7]));
        if (match != NULL) {
            /** Concatena o match na linha */
            strcat(linha, match);

            /** Libera a variavel do match */
            free(match);
        }

        /** Agora, vem o target */
        snprintf(buff, sizeof (buff), "-j %s ", row->cell[8]);

        /** Concatena na linha */
        strcat(linha, buff);

        /** Verifica o redir */
        if (strcmp(row->cell[9], "DNAT") == 0) {
            /*
             * Se entrar aqui, o redirect ip não pode estar vazio 
             * Adiciona na regra o redir
             */
            if (strcmp(row->cell[11], "") == 0) {
                /** Entra aqui se não tiver a porta do redirecionamento */
                snprintf(buff, sizeof (buff), " --to-destination %s ", row->cell[10]);
            } else {
                /** Entra aqui se tiver a porta do redirecionamento */
                snprintf(buff, sizeof (buff), " --to-destination %s:%s ", row->cell[10], row->cell[11]);
            }
            /** Concatena na linha */
            strcat(linha, buff);

        }

        /** No final da linha concatenamos um \n*/
        strcat(linha, "\n");

        /** Faz o write da regra */
        if (write_nat_rules_file(fd, linha) == WRITE_RULE_FILE_FAILED) {
            /** Não conseguiu gravar no arquivo, retorna o erro */
            return GENERATE_NAT_RULES_FAILED;
        }

        /** Se chegar aqui, Vai para a proxima linha */
        row = row->next_line;
    }

    /** Limpa o result set do PG */
    clear_query(&query);

    /** Desaloca o espaço alocado por row */
    free_row(row);

    /** Retorna sucesso */
    return GENERATE_NAT_RULES_SUCCESS;
}

int generate_nat_postrouting_rules(int fd, DBConnection* conn) {
    /*
     * Recupera as regras do banco de dados 
     * Cria um objeto do tipo query
     */
    Query query;

    bool have_proto = false;

    /*
     * Cria a sql 
     * Seleciona as regras por tabela e chain
     */
    strcpy(query.sql, "SELECT rule_index, in_iface, proto, source_addr, source_port, dest_addr, " \
                      "dest_port, id, action, redirect_ip, " \
                      "redirect_port FROM firewall_nat_rules WHERE ativa = true and chain = 'POSTROUTING';");

    /** Executa a query */
    if (execute_query(conn, &query) == QUERY_EXECUTE_FAILED) {
        /** Se entrar, insere no log e retorna erro */
        insert_log(FATAL, LOG_FIREWALL, "Falha na leitura das regras de firewall (nat/POSTROUTING) - nat.c");

        /** Retorna erro */
        return GENERATE_NAT_RULES_FAILED;
    }

    /** Se chegar aqui, faz o fetch das linhas do banco */
    Row * row;
    row = fetch(&query);

    /** Verifica se as linhas não estão null */
    if (row == NULL) {
        /** Insere nos logs */
        insert_log(NOTICE, LOG_FIREWALL, "Nenhuma regra da tabela nat foi lida do banco de dados (nat/POSTROUTING) - nat.c");

        /** Retorna NULL */
        return NO_RULES;
    }
    /** Declara um buffer para a linha */
    char linha[300];
    char buff[50];
    char * match;

    /** Se  as linhas não estiverem nulas, faz um loop para percorre-las */
    while (row->next_line != NULL) {
        /*
         * Começa pela posição da regra 
         * Faz o append, ou insere em alguma posição?
         */
        if (atoi(row->cell[0]) == -1) {
            /** Se é -1, faz o append */
            strcpy(linha, "-A POSTROUTING ");
        } else {
            /** Se não for, insere na posição desejada */
            snprintf(buff, sizeof (buff), "-I POSTROUTING %s ", row->cell[0]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem In_iface */
        if (strcmp(row->cell[1], "") != 0) {
            /** Se entrar aqui tem, então coloca na regra */
            snprintf(buff, sizeof (buff), "--in-interface %s ", row->cell[1]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica agora se tem o protocolo */
        if (strcmp(row->cell[2], "") != 0) {
            /** Se entrar aqui, tem o protocolo, então colcoca na regra*/
            snprintf(buff, sizeof (buff), "--proto %s ", row->cell[2]);

            /** Concatena na linha */
            strcat(linha, buff);

            /** Assinala have_proto como true */
            have_proto = true;
        }

        /** Verifica se tem o src_addr */
        if (strcmp(row->cell[3], "") != 0) {
            /** Se entrar aqui tem */
            snprintf(buff, sizeof (buff), "--source %s ", row->cell[3]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * Verifica se tem o src_port
         * Só tem se existir o proto
         */
        if (strcmp(row->cell[4], "") != 0 && have_proto == true) {
            /** Insere na regra */
            snprintf(buff, sizeof (buff), "--source-port %s ", row->cell[4]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /** Verifica se tem o dest_addr */
        if (strcmp(row->cell[5], "") != 0) {
            /** Se entrar aqui, tem o destination address */
            snprintf(buff, sizeof (buff), "--destination %s ", row->cell[5]);

            /** Concatena na linha */
            strcat(linha, buff);
        }

        /*
         * O destination port funciona da mesma forma que o src port
         * Precisa ter o proto
         */
        if (strcmp(row->cell[6], "") != 0 && have_proto == true) {
            /** Se entrar aqui te, dport, e tem proto */
            snprintf(buff, sizeof (buff), "--destination-port %s ", row->cell[6]);

            /** Concatena a linha */
            strcat(linha, buff);
        }

        /** Gera o match da regra, caso houver */
        match = generate_nat_match(atoi(row->cell[7]));
        if (match != NULL) {
            /** Concatena o match na linha */
            strcat(linha, match);

            /** Libera a variavel do match */
            free(match);
        }

        /** Agora, vem o target */
        snprintf(buff, sizeof (buff), "-j %s ", row->cell[8]);

        /** Concatena na linha */
        strcat(linha, buff);

        /** No final da linha concatenamos um \n*/
        strcat(linha, "\n");

        /** Faz o write da regra */
        if (write_nat_rules_file(fd, linha) == WRITE_RULE_FILE_FAILED) {
            /** Não conseguiu gravar no arquivo, retorna o erro */
            return GENERATE_NAT_RULES_FAILED;
        }

        /** Se chegar aqui, Vai para a proxima linha */
        row = row->next_line;
    }
    /** Limpa o result set do PG */
    clear_query(&query);

    /** Desaloca o espaço alocado por row */
    free_row(row);

    /** Retorna sucesso */
    return GENERATE_NAT_RULES_SUCCESS;
}