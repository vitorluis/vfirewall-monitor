/* 
 * File:   nat.h
 * Author: vfirewall
 *
 * Created on December 9, 2013, 6:24 PM
 */

#ifndef NAT_H
#define	NAT_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include "config_file.h"
#include "database.h"
#include "defines.h"
#include "logger.h"
#include "firewall.h"
#include "match.h"

int init_nat(DBConnection * conn);

int generate_nat_policy(int fd, DBConnection * conn);

int generate_nat_output_rules(int fd, DBConnection * conn);

int generate_nat_prerouting_rules(int fd, DBConnection * conn);

int generate_nat_postrouting_rules(int fd, DBConnection * conn);

int open_nat_rules_file();

int write_nat_rules_file(int fd, const char * content);

void close_nat_rules_file(int fd);

#endif	/* NAT_H */

