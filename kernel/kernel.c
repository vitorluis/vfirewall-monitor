#include "kernel.h"
DBConnection * conn;
config_t conf;

extern int init_kernel() {
    /*
     * Inicia toda a aplicação
     * Registra os signals
     */
    if (register_signals() != REGISTER_SIGNALS_SUCCESS) {
        /** Falha no registro dos signals */
        return INIT_FAILED;
    }

    /* Lê o arquivo de configuração */

    if (read_application_config() == READ_CONFIG_FILE_FAILED)
        return INIT_FAILED;

    /** Registra o PID */
    char pid[100];
    snprintf(pid, sizeof (pid), "echo %d > %s", (int) getpid(), get_config_str(&conf, "pid_file"));
    system(pid);
    
    /** Abre a conexão com o banco de dados */
    /** Aloca memoria */
    conn = malloc(sizeof (DBConnection));

    /** Atribui os valores */
    strcpy(conn->dbname, get_config_str(&conf, "dbname"));
    strcpy(conn->host, get_config_str(&conf, "dbserver"));
    strcpy(conn->user, get_config_str(&conf, "dbuser"));
    strcpy(conn->passwd, get_config_str(&conf, "dbpasswd"));
    conn->port = get_config_int(&conf, "dbport");

    /** Tenta abrir a conexão */
    if (open_connection(conn) == DB_CONNECT_FAILED) {
        /** Insere o erro no log */
        insert_log(FATAL, LOG_KERNEL, "Não foi possivel conectar ao banco de dados - kernel.c");

        /** Retorna falha */
        return INIT_FAILED;
    }

    /*
     * Pega as informações do sistema 
     * Cria uma nova thread para a execução da tarefa
     */

    Thread thread_sysinfo;
    /** Assinala falso para fazer o join */
    thread_sysinfo.detach = false;

    /** Cria a thread chamando a função get_all_system_info */
    create_thread(&thread_sysinfo, get_all_system_info);

    /*
     * Agora inicia o firewall
     */
    Thread thread_firewall;
    thread_firewall.detach = false;

    /** Cria a nova thread chamando a função de init do firewall */
    create_thread(&thread_firewall, init_firewall);

    return INIT_SUCCESS;
}

int get_all_system_info() {
    /** Inicia a conexão com o banco de dados */

    /** TODO: Ler os dados de um arquivo de configuração */
    /*
     * Pega os dados das partições 
     * E salva logo em seguida
     */
    Partition * part;
    part = malloc(sizeof (Partition));

    /** Tenta pegar as informações */
    if (get_partitions_info(part) == GET_PARTITION_INFO_FAILED) {
        /** TODO: Colocar a falha nos logs */
        /*
         * Se entrar aqui, deu falha 
         * Libera memória 
         */
        /** Insere nos logs */
        insert_log(ERROR, LOG_KERNEL, "Falha ao recuperar os dados das partições - kernel.c");

        /** Libera memoria */
        free_partitions(part);

        /** Retorna falha */
        return GET_ALL_INFO_FAILED;
    } else {
        /*
         * Chegando aqui, conseguiu pegar os dados
         * Agora tenta salvar
         */
        if (save_partitions_info(conn, part) == SAVE_INFO_IFACES_FAILED) {
            /** Se entrar aqui, deu falha no salvar */

            /** Insere nos logs */
            insert_log(ERROR, LOG_KERNEL, "Falha ao salvar os dados das partições - kernel.c");

            /** Libera memória */
            free_partitions(part);

            /** Retorna falha */
            return GET_ALL_INFO_FAILED;
        }
    }

    free_partitions(part);

    /** Identifica as interfaces de rede */
    Interface * ifaces;
    ifaces = malloc(sizeof (Interface));
    ifaces->next_interface = NULL;

    if (get_interfaces_info(ifaces) == GET_INFO_IFACES_FAILED) {
        /** Insere nos logs */
        insert_log(ERROR, LOG_KERNEL, "Falha ao recuperar os dados das interfaces - kernel.c");

        /** Libera memória */
        free_interfaces(ifaces);

        /** Retorna falha */
        return GET_ALL_INFO_FAILED;
    } else {
        /*
         * Se chegar aqui, conseguiu pegar os dados das interfaces 
         * Então salva os dados capturados
         */
        if (save_interfaces_info(conn, ifaces) == SAVE_INFO_IFACES_FAILED) {
            /** Insere nos logs */
            insert_log(ERROR, LOG_KERNEL, "Falha ao salvar os dados das interfaces - kernel.c");

            /** Libera memória */
            free_interfaces(ifaces);

            /** Caso não conseguiur salvar retorna falha*/
            return GET_ALL_INFO_FAILED;
        }
        /** Agora libera os dados da memória */
        free_interfaces(ifaces);
    }

    /** Pega os dados da memoria  e do swap */
    Memory * mem;
    mem = malloc(sizeof (Memory));

    Swap * swap;
    swap = malloc(sizeof (Swap));

    /** Tenta pegar os dados da memoria RAM */
    if (get_memory_info(mem) == GET_MEM_INFO_FAILED || get_swap_info(swap) == GET_SWAP_INFO_FAILED) {
        /** Se entrar aqui, não conseguiu pegar os dados */

        /** Insere nos logs */
        insert_log(ERROR, LOG_KERNEL, "Falha ao recuperar os dados da RAM e Swap - kernel.c");

        /** Libera a memória */
        free(mem);
        free(swap);

        /** Retorna falha */
        return GET_ALL_INFO_FAILED;
    } else {
        /*
         * Entrando aqui, conseguiu pegar os dados 
         * Agora tenta salvar
         */
        if (save_memory_info(conn, mem, swap) == SAVE_MEMORY_INFO_FAILED) {
            /** Se entrar aqui, falhou no salvar */

            /** Insere nos logs */
            insert_log(ERROR, LOG_KERNEL, "Falha ao salvar os dados da RAM e Swap - kernel.c");

            /** Libera memória */
            free(mem);
            free(swap);

            /** Retorna falha */
            return GET_ALL_INFO_FAILED;
        }
        /** Libera memória */
        free(mem);
        free(swap);
    }


    /** Pega os dados do sistema */
    Sysinfo * sys;
    sys = malloc(sizeof (Sysinfo));

    /** Tenta pegar os dados do sistema */
    if (get_system_info(sys) == GET_SYSINFO_FAILED) {
        /** Se entrar aqui, não conseguiu pegar os dados */

        /** Insere nos logs */
        insert_log(ERROR, LOG_SYSINFO, "Falha ao recuperar os dados do sistema - sysinfo.c");
        insert_log(ERROR, LOG_KERNEL, "Falha ao recuperar os dados do sistema - kernel.c");
        /** Libera a memória */
        free(sys);

        /** Retorna falha */
        return GET_ALL_INFO_FAILED;
    } else {
        /*
         * Entrando aqui, conseguiu pegar os dados 
         * Agora tenta salvar
         */
        if (save_system_info(conn, sys) == SAVE_SYSINFO_FAILED) {
            /** Se entrar aqui, falhou no salvar */

            /** Insere nos logs */
            insert_log(ERROR, LOG_SYSINFO, "Falha ao salvar os dados do sistema - kernel.c");
            insert_log(ERROR, LOG_KERNEL, "Falha ao salvar os dados do sistema - kernel.c");

            /** Libera memória */
            free(sys);

            /** Retorna falha */
            return GET_ALL_INFO_FAILED;
        }
        /** Libera memória */
        free(sys);
    }

    /** Por ultimo os dados da CPU */
    CPU * cpu;
    cpu = malloc(sizeof (CPU));

    /** Tenta pegar os dados */
    if (get_cpu_info(cpu) == GET_CPU_INFO_FAILED) {
        /*
         * Se entrar aqui, deu falha na captura dos dados 
         * Libera memória 
         * 
         */

        /** Insere nos logs */
        insert_log(ERROR, LOG_SYSINFO, "Falha ao recuperar os dados da CPU - sysinfo.c");

        free(cpu);

        /** Retorna falha */
        return GET_ALL_INFO_FAILED;
    } else {
        /*
         * Se chegar aqui, conseguiu pegar os dados 
         * Tenta salvar os dados
         */
        if (save_cpu_info(conn, cpu) == SAVE_CPU_INFO_FAILED) {
            /*
             * Se entrar aqui não conseguiu salvar
             * Libera memória 
             */

            /** Insere nos logs */
            insert_log(ERROR, LOG_SYSINFO, "Falha ao salvar os dados da CPU - kernel.c");
            insert_log(ERROR, LOG_KERNEL, "Falha ao salvar os dados da CPU - kernel.c");

            free(cpu);

            /** Retorna falha */
            return GET_ALL_INFO_FAILED;
        }
        /** Libera Memória */
        free(cpu);
    }

    /*
     * Chegando aqui, conseguiu pegar todos os dados 
     * Retorna sucesso
     */

    return GET_ALL_INFO_SUCCESS;
}

int read_application_config() {
    /** Chama a função que faz a leitura */
    if (read_config_file(&conf) == READ_CONFIG_FILE_FAILED)
        return READ_CONFIG_FILE_FAILED;

    /** Se chegar aqui, conseguiu ler */
    return READ_CONFIG_FILE_SUCCESS;
}

int init_firewall() {
    /** Chama o inicio do firewall */
    if (init_firewall_rules(conn) == INIT_FIREWALL_FAILED) {
        /*
         * Se entrar aqui não conseguiu gerar as regras e aplica-las 
         * Insere nos logs 
         */
        insert_log(FATAL, LOG_KERNEL, "Falha na inicialização do firewall - kernel.c");

        /** Retorna falha */
        return INIT_FIREWALL_FAILED;
    }
    /** Se iniciou, retorna sucesso */
    return INIT_FIREWALL_SUCCESS;
}

int reload_system() {
    /** fecha o arquivo de configuração */
    close_config_file(&conf);

    /** Fecha a conexão com o banco */
    close_connection(conn);
    
    /** Libera a conn */
    free(conn);

    /** Faz o init do kernel novamente */
    if (init_kernel() == INIT_FAILED) {
        return INIT_FAILED;
    } else {
        return INIT_SUCCESS;
    }
}

int register_signals() {
    /** Monta a struct que vai fazer o handle */
    struct sigaction sa;

    /** Seta qual é a função que vai fazer o handle */
    sa.sa_handler = &handle_signals;

    /** Restarta as system calls */
    sa.sa_flags = SA_RESTART;

    /** Bloqueia os outros signals durante a manipulação do signal */
    sigfillset(&sa.sa_mask);

    /** Registra os signals necessários */
    if (sigaction(SIGHUP, &sa, NULL) == -1) {
        /** Falha no registro do signal, insere nos logs */
        insert_log(ERROR, LOG_KERNEL, "Falha no registro do signal SIGHUP - kernel.c");

        /** Retorna a falha */
        return REGISTER_SIGNALS_FAILED;
    }

    if (sigaction(SIGINT, &sa, NULL) == -1) {
        /** Falha no registro do signal, insere nos logs */
        insert_log(ERROR, LOG_KERNEL, "Falha no registro do signal SIGINT - kernel.c");

        /** Retorna a falha */
        return REGISTER_SIGNALS_FAILED;
    }

    if (sigaction(SIGTERM, &sa, NULL) == -1) {
        /** Falha no registro do signal, insere nos logs */
        insert_log(ERROR, LOG_KERNEL, "Falha no registro do signal SIGTERM - kernel.c");

        /** Retorna a falha */
        return REGISTER_SIGNALS_FAILED;
    }

    /** Se deu tudo certo, retorna sucesso */
    return REGISTER_SIGNALS_SUCCESS;
}

void handle_signals(int signal) {
    /** Verifica qual foi o signal enviado e faz o tratamento correto */
    switch (signal) {
        case SIGHUP:
            reload_system();
            break;
        case SIGINT:
            finish_system();
            break;
        case SIGTERM:
            finish_system();
            break;
    }
}

void finish_system() {
    /** Fecha a conexão */
    close_connection(conn);

    /** Fecha o arquivo de configuração */
    close_config_file(&conf);

    /** Faz o flusb das regras */
    flush_rules();

    /** Sai do sistema */
    exit(EXIT_SUCCESS);
}