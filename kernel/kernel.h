/* 
 * File:   kernel.h
 * Author: vitor
 *
 * Created on October 1, 2013, 5:29 PM
 */

#ifndef KERNEL_VFIREWALL_H
#define	KERNEL_VFIREWALL_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include "defines.h"
#include "database.h"
#include "interfaces.h"
#include "memory.h"
#include "partitions.h"
#include "sysinfo.h"
#include "config_file.h"
#include "logger.h"
#include "thread.h"
#include "firewall.h"
#include "command.h"
/** Funções */

extern int init_kernel();

int read_application_config();

int init_logger();

int get_all_system_info();

int init_firewall();

int reload_system();

int register_signals();

void handle_signals(int signal);

void finish_system();
#endif	/* KERNEL_VFIREWALL_H */
