#include "server.h"
config_t conf;

/*
 * Ira fazer o tratamento
 * dos eventos
 */

int init_socket() {
    /** Declara um socket */
    Socket sock;
    Command * command;

    /** Inicia o socket */
    sock.socket = socket(AF_INET, SOCK_STREAM, 0);

    /** Seta zeros no sockaddr */
    memset(&sock.server, 0, sizeof (sock.server));
    memset(&sock.client, 0, sizeof (sock.client));

    /** E tambem no buffer */
    memset(sock.buff, 0, sizeof (sock.buff));

    /** Seta os valores do sockaddr */
    sock.server.sin_family = AF_INET;
    sock.server.sin_addr.s_addr = htonl(INADDR_ANY);
    sock.server.sin_port = htons(get_config_int(&conf, "monitorport"));
    //sock.server.sin_port = htons(2200);

    /** Chama o bind */
    bind(sock.socket, (struct sockaddr*) &sock.server, sizeof (sock.server));

    /*
     * É um socket blocante, então espera encher o buffer 
     * Faz o listen
     */
    if (listen(sock.socket, 2) == -1) {
        /** Deu falha na preparação para o accept, insere nos logs */
        insert_log(FATAL, LOG_KERNEL, "Não foi possível fazer o listen - server.c");

        /** Retorna falha */
        return INIT_SOCKET_FAILED;
    }

    /** Se chegar aqui, faz o accept, dentro de um loop infinito */
    socklen_t client_len = sizeof (sock.client);
    while ((sock.conn = accept(sock.socket, (struct sockaddr*) &sock.client, &client_len))) {

        /** Insere nos logs quem conectou no socket */
        char log[50];
        snprintf(log, sizeof (log), "O IP %s conectou - server.c", inet_ntoa(sock.client.sin_addr));
        insert_log(NOTICE, LOG_CONNECTION, log);

        /** Agora conn é um file descriptor, podemos ler e gravar nele */
        while (true) {
            if (read(sock.conn, sock.buff, sizeof (sock.buff)) == 0) {
                /*
                 * Como é um socket blocante, se não ler nada do socket
                 * quer dizer que o cliente desconectou
                 */
                close(sock.conn);
                break;
            }
            /** Cria uma command */
            command = malloc(sizeof (Command));

            /** Chama o inicio do comando */
            if (process_command(command, sock.buff) == COMMAND_FAILED) {
                /** Deu erro no comando, envia qual foi a mensagem de erro*/
                write_in_socket(command->error, &sock);
                
            } else {
                /** Tudo OK com o comando, envia o resultado */
                write_in_socket(command->result, &sock);
            }

            /** Fim da execução do comando, Limpa o buffer */
            memset(sock.buff, 0, sizeof (sock.buff));

            /** Libera memoria */
            free(command);
            
            /** Dorme um segundo para nao topar o processador */
            sleep(1);
        }
    }

    return INIT_SOCKET_SUCCESS;
}

int write_in_socket(char* msg, Socket * sock) {
    /** Faz o write da mensagem no socket */
    if (write(sock->conn, msg, strlen (msg)) == -1) {
        /** Falha ao escrever no socket, retorna falha */
        return WRITE_FAILED;
    } else {
        /** Conseguiu escrever, retorna sucesso */
        return WRITE_SUCCESS;
    }
}

int process_command(Command * command, char * str_command) {
    /** Chama o comando */
    if (init_command((const char *) str_command, command) == COMMAND_FAILED) {
        /** Falha na execução do comando, retorna a falha */
        return COMMAND_FAILED;
    } else {
        /** Comando executadocom sucesso, retorna sucesso */
        return COMMAND_SUCCESS;
    }
}