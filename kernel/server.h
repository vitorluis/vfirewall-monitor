/* 
 * File:   event.h
 * Author: vfirewall
 *
 * Created on October 17, 2013, 8:21 PM
 */

#ifndef EVENT_H
#define	EVENT_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/time.h>
#include "config_file.h"
#include "logger.h"
#include "command.h"
#include "thread.h"

/** Cosntantes do socket */
#define WRITE_SUCCESS 500
#define WRITE_FAILED 501

/*
 * O evento tratará os comandos que virão via socket
 * O comando será processado e depois será enviado a resposta
 */
typedef struct socket {
    int socket;
    int conn;
    char buff[4096];
    struct sockaddr_in server;
    struct sockaddr_in client;
} Socket;


int init_socket();

int write_in_socket(char * msg, Socket * sock);

int process_command(Command * command, char * str_command);
#endif	/* EVENT_H */

