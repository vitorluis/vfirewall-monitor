/* 
 * File:   main.c
 * Author: vitor
 *
 * Created on September 16, 2013, 9:06 PM
 * 
 * Esse é o main, o programa que vai iniciar o monitor
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "kernel.h"
#include "logger.h"
#include "thread.h"
#include "server.h"
#include "json.h"

int main(int argc, char** argv) {

    /** Inicializa o daemon */
    daemon(0, fileno(stdout));

    /** Faz o init do kernel */
    if (init_kernel() != INIT_SUCCESS) {
        /** Falha na inicialização do kernel */
        return EXIT_FAILURE;
    }

    /** Inicia o socket */
    init_socket();
    return (EXIT_SUCCESS);
}

