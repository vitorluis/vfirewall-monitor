/* 
 * File:   defines.h
 * Author: vitor
 *
 * Created on September 16, 2013, 9:25 PM
 * 
 * Arquivo com os Defines que serão utilizados no monitor
 */

#ifndef DEFINES_H
#define	DEFINES_H

//Status
#define INTERFACE_ACTIVE                     500
#define INTERFACE_INACTIVE                   501

//Sucessos
#define GET_INFO_IFACES_SUCCESS              100
#define GET_IPADDR_SUCCESS                   101
#define GET_NETMASK_SUCCESS                  102
#define GET_BROADCAST_SUCCESS                103
#define GET_MAC_ADDR_SUCCESS                 104
#define SAVE_INFO_IFACES_SUCCESS             105
#define DB_CONNECT_SUCCESS                   106
#define QUERY_EXECUTE_SUCCESS                107
#define GET_MEM_INFO_SUCCESS                 108
#define GET_SWAP_INFO_SUCCESS                109
#define SAVE_MEMORY_INFO_SUCCESS             110
#define GET_INFO_PARTITION_SUCCESS           111
#define PARSE_MTAB_SUCCESS                   112
#define REGEX_COMPILE_SUCCESS                113
#define GET_PARTITION_SPACE_SUCCESS          114
#define GET_PARTITION_INFO_SUCCESS           115
#define SAVE_PARTITION_INFO_SUCCESS          116
#define GET_CPU_INFO_SUCCESS                 117
#define GET_SYSINFO_SUCCESS                  118
#define SAVE_CPU_INFO_SUCCESS                119
#define SAVE_SYSINFO_SUCCESS                 120
#define GET_ALL_INFO_SUCCESS                 121
#define READ_CONFIG_FILE_SUCCESS             122
#define INSERT_LOG_SUCCESS                   123
#define THREAD_CREATE_SUCCESS                126
#define WRITE_RULE_FILE_SUCCESS              129
#define GENERATE_FILTER_RULES_SUCCESS        130
#define GENERATE_FILTER_POLICY_SUCCESS       131
#define GENERATE_MANGLE_POLICY_SUCCESS       132
#define GENERATE_MANGLE_RULES_SUCCESS        133
#define GENERATE_NAT_POLICY_SUCCESS          134
#define GENERATE_NAT_RULES_SUCCESS           135
#define INIT_FIREWALL_SUCCESS                136
#define GENERATE_RULES_SUCCESS               137
#define APPLY_RULES_SUCCESS                  138
#define INIT_SOCKET_SUCCESS                  139
#define IPTABLES_FLUSH_SUCCESS               140
#define SET_CONFIG_SUCCESS                   141
#define LOAD_FILE_RULES_SUCCESS              142
#define RELOAD_SYSTEM_SUCCESS                143
#define INIT_SUCCESS                         144
#define REGISTER_SIGNALS_SUCCESS             145

//Erros
#define GET_INFO_IFACES_FAILED              -100
#define GET_IFADDR_FAILED                   -101
#define GET_NETMASK_FAILED                  -102
#define GET_BROADCAST_FAILED                -103
#define GET_MAC_ADDR_FAILED                 -104
#define SAVE_INFO_IFACES_FAILED             -105
#define DB_CONNECT_FAILED                   -106
#define QUERY_EXECUTE_FAILED                -107
#define GET_MEM_INFO_FAILED                 -108
#define GET_SWAP_INFO_FAILED                -109
#define SAVE_MEMORY_INFO_FAILED             -110
#define GET_INFO_PARTITION_FAILED           -111
#define PARSE_MTAB_FAILED                   -112
#define REGEX_COMPILE_FAILED                -113
#define GET_PARTITION_SPACE_FAILED          -114
#define GET_PARTITION_INFO_FAILED           -115
#define SAVE_PARTITION_INFO_FAILED          -116
#define GET_CPU_INFO_FAILED                 -117
#define GET_SYSINFO_FAILED                  -118
#define SAVE_CPU_INFO_FAILED                -119
#define SAVE_SYSINFO_FAILED                 -120
#define GET_ALL_INFO_FAILED                 -121
#define READ_CONFIG_FILE_FAILED             -122
#define INIT_FAILED                         -123
#define OPEN_LOG_FILE_FAILED                -124
#define INSERT_LOG_FAILED                   -125
#define THREAD_CREATE_FAILED                -126
#define READ_FIREWALL_RULES_FAILED          -127
#define OPEN_RULES_FILE_FAILED              -128
#define WRITE_RULE_FILE_FAILED              -129
#define GENERATE_FILTER_RULES_FAILED        -130
#define GENERATE_FILTER_POLICY_FAILED       -131
#define GENERATE_MANGLE_POLICY_FAILED       -132
#define GENERATE_MANGLE_RULES_FAILED        -133
#define GENERATE_NAT_POLICY_FAILED          -134
#define GENERATE_NAT_RULES_FAILED           -135
#define INIT_FIREWALL_FAILED                -136
#define GENERATE_RULES_FAILED               -137
#define APPLY_RULES_FAILED                  -138
#define INIT_SOCKET_FAILED                  -139
#define IPTABLES_FLUSH_FAILED               -140
#define SET_CONFIG_FAILED                   -141
#define LOAD_FILE_RULES_FAILED              -142
#define RELOAD_SYSTEM_FAILED                -143
#define REGISTER_SIGNALS_FAILED             -145

//Tabelas do Firewall
#define TABLE_FILTER                         510
#define TABLE_MANGLE                         511
#define TABLE_NAT                            512
#define NO_RULES                             513

/** COMANDOS */
/** Constantes que informam sucesso ou falha dos conandos */
#define COMMAND_SUCCESS                      1000
#define COMMAND_FAILED                       1001
#define DECODE_COMMAND_SUCCESS               1002
#define DECODE_COMMAND_FAILED                1003
#define COMMAND_EXISTS                       1004
#define COMMAND_NOT_EXISTS                   1005
#define SUBCOMMAND_SUCCESS                   1006
#define SUBCOMMAND_FAILED                    1007
#define SUBCOMMAND_EXISTS                    1008
#define SUBCOMMAND_NOT_EXISTS                1009
#endif	/* DEFINES_H */

