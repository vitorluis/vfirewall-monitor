#include "json.h"

JSON * json_decode(const char * str_json) {
    /** Declara as variaveis */
    JSON * json;
    json_t * root, * obj;
    const char * key;

    /** Faz o load do JSON */
    root = json_loads(str_json, 0, NULL);

    if (!root) {
        /** Falha ao fazer o parse */
        return NULL;
    }
    /** Aloca o JSON */
    json = malloc(sizeof (JSON));
    json->pos = 0;

    /** Faz um loop, para pegar todos os objetos */
    json_object_foreach(root, key, obj) {
        /** Pega o objeto */
        obj = json_object_get(root, key);

        if (strcmp(key, "command") == 0) {
            /** Copia a key */
            strcpy(json->keys[0], key);

            /** Copia o valor */
            strcpy(json->values[0], json_string_value(obj));

            /** Incrementa um na posição */
            json->pos++;
        } else if (strcmp(key, "subcommand") == 0) {
             /** Copia a key */
            strcpy(json->keys[1], key);

            /** Copia o valor */
            strcpy(json->values[1], json_string_value(obj));

            /** Incrementa um na posição */
            json->pos++;
        } else {
             /** Copia a key */
            strcpy(json->keys[json->pos], key);

            /** Copia o valor */
            strcpy(json->values[json->pos], json_string_value(obj));

            /** Incrementa um na posição */
            json->pos++;
        }
    }

    /** Retorna o objeto JSON */
    return json;
}

const char * json_encode(JSON* json) {
    /** Declara as variaveis */
    char * str_json;
    char buff[50];
    int i;

    /** Começa a montar o formato da string do json */
    str_json = malloc(300);
    strcpy(str_json, "{");

    for (i = 0; i <= json->pos; i++) {
        /** Monta a key e o valor */
        if (i == json->pos)
            snprintf(buff, sizeof (buff), "\"%s\":\"%s\"", json->keys[i], json->values[i]);
        else
            snprintf(buff, sizeof (buff), "\"%s\":\"%s\", ", json->keys[i], json->values[i]);

        /** Concatena a key e o valor */
        strcat(str_json, buff);
    }
    /** No final fecha o } */
    strcat(str_json, "}");

    /** Retorna o json encodado */
    return (const char *) str_json;
}