/* 
 * File:   json.h
 * Author: vitor
 *
 * Created on February 20, 2014, 11:33 AM
 */

#ifndef JSON_H
#define	JSON_H
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <jansson.h>
#include <jansson_config.h>
#include "command.h"

/** Cria struct com as keys e values */
typedef struct json {
    char keys[100][100];
    char values[100][100];
    int  pos;
} JSON;

/** Faz o decode da string que vier */
JSON * json_decode(const char * str_json);

const char * json_encode(JSON * json);
#endif	/* JSON_H */

