#include "logger.h"
config_t conf;

int open_log_file(const char* file) {
    int fd;

    if (access(file, F_OK)) {
        /*
         * Se o arquivo não existe 
         * cria ele;
         */
        fd = creat(file, O_WRONLY | O_APPEND);
        chmod(file, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
    } else {
        /*
         * Tenta abrir o arquivo 
         */
        fd = open(file, O_WRONLY | O_APPEND);
    }

    /** Verifica se houve falha ao abrir o arquivo.*/
    if (fd == -1)
        return OPEN_LOG_FILE_FAILED;
    else
        return fd;
}

int insert_log(int level, int category, const char* msg) {
    /** Variavel que guardará toda a mensagem do log */
    char all_message[500];
    strcpy(all_message, "");

    /** Variavel que guardará o nome do arquivo */
    char filename[50];
    strcpy(filename, read_log_path());

    /** Verifica o nivel do log */
    if (level == NOTICE) {
        strcat(all_message, "[NOTICE]");
    } else if (level == WARNING) {
        strcat(all_message, "[WARNING]");
    } else if (level == ERROR) {
        strcat(all_message, "[ERROR]");
    } else if (level == FATAL) {
        strcat(all_message, "[FATAL]");
    }

    /** Pega o horário do sistema */
    FILE * ftime = popen("/bin/date +'[%d-%m-%Y %T] - '", "r");
    char time[30];
    if (fread(time, sizeof (char), sizeof (time), ftime) < 0)
        return INSERT_LOG_FAILED;

    /** Se chegar aqui, conseguiu ler a saída do comando */
    time[sizeof (time) - 6] = '\0';
    fclose(ftime);

    /*
     * Concatena o horário na mensagem 
     */
    strcat(all_message, (const char *) time);

    /** Agora concatena a mensagem */
    strcat(all_message, (const char *) msg);

    /** Concatena no final da mensagem um \n */
    if (all_message[strlen(all_message) - 1] != '\n')
        strcat(all_message, "\n");

    /*
     * Nesse momento, verifica qual é a categoria do log 
     * Se é um log sobre as partições, sobre a cpu
     * sobre o firewall e etc
     */
    if (category == LOG_DATABASE) {
        strcat(filename, "/database.log");
    } else if (category == LOG_INTERFACES) {
        strcat(filename, "/interfaces.log");
    } else if (category == LOG_SYSINFO) {
        strcat(filename, "/sysinfo.log");
    } else if (category == LOG_KERNEL) {
        strcat(filename, "/kernel.log");
    } else if (category == LOG_FIREWALL) {
        strcat(filename, "/firewall.log");
    } else if (category == LOG_CONNECTION) {
        strcat(filename, "/connection.log");
    } else {
        strcat(filename, "/general.log");
    }

    /** Abre o arquivo */
    int fd = open_log_file(filename);

    /*
     * Em caso de falha na abertura do arquivo 
     * retorna falha
     */
    if (fd == -1)
        return INSERT_LOG_FAILED;

    /** Faz o write */
    if (write(fd, all_message, strlen(all_message)) < 0)
        return INSERT_LOG_FAILED;

    /** Fecha o arquivo */
    close_log_file(fd);

    /** Retorna sucesso */
    return INSERT_LOG_FAILED;
}

void close_log_file(int fd) {

    /** Fecha o arquivo */
    close(fd);
}

const char * read_log_path() {
    const char * log_path;
    log_path = get_config_str(&conf, "logpath");
    return log_path;
}