/* 
 * File:   logger.h
 * Author: fastway
 *
 * Created on October 3, 2013, 12:42 PM
 */

#ifndef LOGGER_H
#define	LOGGER_H
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>
#include <locale.h>
#include "config_file.h"
#include "defines.h"

/*
 * Definição de algumas constantes
 * que serão as categorias de logs
 * como por exemplo, log sobre as partições, interfaces e etc
 */
#define LOG_INTERFACES 200
#define LOG_SYSINFO 201
#define LOG_KERNEL 202
#define LOG_DATABASE 203
#define LOG_GENERAL 204
#define LOG_FIREWALL 205
#define LOG_CONNECTION 206

/** Definição de levels de logs */
#define NOTICE 300
#define WARNING 301
#define ERROR 302
#define FATAL 303

int open_log_file(const char * file);

int insert_log(int level, int category, const char * msg);

void close_log_file(int fd);

const char * read_log_path();

#endif	/* LOGGER_H */

