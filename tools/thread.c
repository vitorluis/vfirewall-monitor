#include "thread.h"


int create_thread(Thread* thread, void* func_addr) {
    /** Cria uma nova thread */
    if (pthread_create(&thread->thread_id, NULL, func_addr, NULL) != 0) {
        /** Insere nos logs */
        insert_log(ERROR,LOG_GENERAL,"Falha na criação da thread - thread.c");
        
        /** Retorna falha */
        return THREAD_CREATE_FAILED;
    }
    
    /** Verifica aqui se é pra fazer o join ou o detach */
    if (thread->detach == true) {
        /** Faz o detach */
        thread_detach(thread);
    } else {
        /** Faz o join */
        thread_join(thread);
    }
    
    /** Se chegar aqui conseguiu iniciar a thread */
    return THREAD_CREATE_SUCCESS;
}

void thread_join(Thread* thread) {
    /** Faz o join da thread */
    pthread_join(thread->thread_id, &thread->return_value);
}

void thread_detach(Thread* thread) {
    /** Faz o detach da thread */
    pthread_detach(thread->thread_id);
}