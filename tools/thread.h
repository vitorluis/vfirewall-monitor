/* 
 * File:   thread.h
 * Author: vitor
 *
 * Created on October 12, 2013, 12:48 PM
 */

#ifndef THREAD_H
#define	THREAD_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include "defines.h"
#include "logger.h"

typedef struct thread {
    pthread_t thread_id;
    void * return_value;
    void * arg;
    int flags;
    bool detach;
} Thread;

int create_thread(Thread * thread, void * func_addr);

void thread_join(Thread * thread);

void thread_detach(Thread * thread);


#endif	/* THREAD_H */

