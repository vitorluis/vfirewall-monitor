#include "tools.h"

bool is_int(const char* str) {
    /** Pega o tamanho da string e faz um loop para rodar todos os chars */
    int i, tamanho = strlen(str) - 1;
    for (i = 0; i <= tamanho; i++) {
        if (!isdigit(str[i]))
            return false;
    }
    return true;
}

bool is_float(const char* str) {
    /** Pega o tamanho da string e faz um loop para rodar todos os chars */
    int i, tamanho = strlen(str) - 1;
    for (i = 0; i <= tamanho; i++) {
        if (!isdigit(str[i])) {
            if (str[i] != '.')
                return false;
        }
    }
    return true;
}

bool is_valid_ip(const char * ip) {
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ip, &(sa.sin_addr));
    return result != 0;
}

char * execute_command(const char* command) {
    FILE *fp;
    char * output;
    output = malloc(sizeof(char) * 1024);

    /* Open the command for reading. */
    fp = popen(command, "r");
    if (fp == NULL) {
        return NULL;
    }

    /* Read the output a line at a time - output it. */
    if (fgets(output, 1023, fp) != NULL) {
        /* close */
        pclose(fp);
        
        /** retorna a saída */
        return output;
    } else {
        /* close */
        pclose(fp);
        
        /** retorna a saída */
        return NULL;
    }

}