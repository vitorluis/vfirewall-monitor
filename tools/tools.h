/* 
 * File:   tools.h
 * Author: vfirewall
 *
 * Created on February 4, 2014, 7:10 PM
 */

#ifndef TOOLS_H
#define	TOOLS_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/types.h>
#include <arpa/inet.h>

/*
 * Funções helpers para toda a aplicação 
 * A maioria das funções são de validação
 */

/** Verifica se é um int */
bool is_int(const char * str);

/** Verifica se é float */
bool is_float(const char * str);

/** Verifica se é um ip valido */
bool is_valid_ip(const char * ip);

/** Executa um comando e retorna o output */
char * execute_command(const char * command);
#endif	/* TOOLS_H */

